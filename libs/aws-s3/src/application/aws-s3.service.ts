import { Injectable } from '@nestjs/common';
import * as AWS from 'aws-sdk';
import * as mime from 'mime-types';

@Injectable()
export class AwsS3Service {
  private accessKey: string;
  private secretKey: string;

  public setCredentials(accessKey: string, secretKey: string): AwsS3Service {
    this.accessKey = accessKey;
    this.secretKey = secretKey;
    return this;
  }

  public uploadFromBuffer(file: any, path: string, uploader: string, bucket: string) {
    const filename = this.generateFileName(file, uploader);
    return this.storeFileBufferInS3(
      `${path}/${filename}`,
      Buffer.from(file.buffer, 'base64'),
      bucket,
    );
  }

  private generateFileName(file: any, uploader: string) {
    let ext = file.originalname.split('.');
    ext = ext[ext.length - 1];
    return `${uploader}-${Date.now()}.${ext}`;
  }

  private storeFileBufferInS3(path: string, content: Buffer, bucket: string) {
    return this.storeFrom64({
      path: path,
      bucket: bucket,
      Content: content,
    });
  }

  private storeFrom64(fileObject) {
    const { accessKey, secretKey } = this;
    return new Promise((resolve, reject) => {
      const s3 = new AWS.S3({
        accessKeyId: accessKey,
        secretAccessKey: secretKey,
      });
      const bucket = fileObject.bucket;

      const uploadContent = () => {
        const params = {
          Bucket: bucket,
          Key: fileObject.path,
          Body: fileObject.Content,
          ACL: 'public-read',
          ContentType: mime.lookup(fileObject.path),
        };
        s3.upload(params, function (error, data) {
          if (error) {
            reject(error);
          }
          resolve(data);
        });
      };
      uploadContent();
    });
  }
}
