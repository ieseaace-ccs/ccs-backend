[![Quality Gate Status](https://sonarcloud.io/api/project_badges/measure?project=ieseaace-ccs_ccs-backend&metric=alert_status)](https://sonarcloud.io/dashboard?id=ieseaace-ccs_ccs-backend)

## Description

This is the monorepo for CCS Backend & Microservices

## Installation

```bash
$ yarn install
```

## Running the app

```bash
# development
$ yarn start

# watch mode
$ yarn start:dev

# production mode
$ yarn start:prod
```

## Test

```bash
# unit tests
$ yarn test

# e2e tests
$ yarn test:e2e

# test coverage
$ yarn test:cov
```
