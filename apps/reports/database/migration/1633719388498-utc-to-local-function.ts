import { MigrationInterface, QueryRunner } from 'typeorm';

export class utcToLocalFunction1633719388498 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`CREATE FUNCTION switchUTCtoLocal
    (
        @DATE DATETIME
    )
    RETURNS DATETIME
    AS
    BEGIN
        DECLARE @PARSED_DATE DATETIME
        SELECT @PARSED_DATE = DATEADD(mi, DATEDIFF(mi, GETUTCDATE(), GETDATE())-60, @DATE)
        RETURN @PARSED_DATE
    END`);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`DROP FUNCTION switchUTCtoLocal`);
  }
}
