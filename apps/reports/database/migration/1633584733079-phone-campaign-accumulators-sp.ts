import { MigrationInterface, QueryRunner } from 'typeorm';

export class phoneCampaignAccumulatorsSp1633584733079
  implements MigrationInterface
{
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`CREATE PROCEDURE campaignAccumulators
            
        --DECLARE 
            @INTERVALO INT,
            @AGRUPADO INT,
            @TOTALIZADO INT,
            @CAMPAIGN VARCHAR(MAX) ,
            @SKILL VARCHAR(MAX),
            @FECHA_INI DATETIME = NULL,
            @FECHA_FIN DATETIME = NULL

        --SET @INTERVALO = 4
        --SET @AGRUPADO = 3
        --SET @TOTALIZADO = 1
        --SET @CAMPAIGN = 'tcl||'
        --SET @SKILL = -1
        --SET @FECHA_INI = '01/10/2020'
        --SET @FECHA_FIN = '05/10/2020'

        AS BEGIN

            SET DATEFIRST 1

            DECLARE @INT_FECHA_INI DATETIME, @INT_FECHA_FIN DATETIME

            IF @INTERVALO = 0 BEGIN

                SET @INT_FECHA_INI = DATEADD(dd,DATEDIFF(dd,0,GETDATE()),0)
                SET @INT_FECHA_FIN = DATEADD(ms,-3,DATEADD(dd,DATEDIFF(dd,0,GETDATE()),1))

            END

            IF @INTERVALO = 1 BEGIN

                SET @INT_FECHA_INI = DATEADD(wk,DATEDIFF(wk,0,GETDATE()),0)
                SET @INT_FECHA_FIN = DATEADD(wk,DATEDIFF(wk,0,GETDATE()),7)

            END

            IF @INTERVALO = 2 BEGIN

                SET @INT_FECHA_INI = DATEADD(mm,DATEDIFF(mm,0,GETDATE()),0)
                SET @INT_FECHA_FIN = DATEADD(ms,-3,DATEADD(mm,0,DATEADD(mm,DATEDIFF(mm,0,GETDATE())+1,0)))

            END

            IF @INTERVALO = 3 BEGIN

                SET @INT_FECHA_INI = DATEADD(yy,DATEDIFF(yy,0,GETDATE()),0)
                SET @INT_FECHA_FIN = DATEADD(ms,-3,DATEADD(yy,0,DATEADD(yy,DATEDIFF(yy,0,GETDATE())+1,0)))

            END

            IF @INTERVALO = 4 BEGIN

                SET @INT_FECHA_INI = @FECHA_INI
                SET @INT_FECHA_FIN = @FECHA_FIN + 1

            END

            DECLARE @tmpCampanias AS TABLE(Campania VARCHAR(MAX))
            INSERT INTO @tmpCampanias
                (Campania)
            SELECT idCampo
            FROM Reports.dbo.multiSelect(@CAMPAIGN)

            DECLARE @tmpSkills AS TABLE(Skill VARCHAR(MAX))
            INSERT INTO @tmpSkills
                (Skill)
            SELECT idCampo
            FROM Reports.dbo.multiSelect(@SKILL)



            SELECT

                CASE WHEN @TOTALIZADO = 1 THEN 'Todos' ELSE b.campaign END as Campania,
                CASE WHEN @TOTALIZADO =1 THEN 'Todos' ELSE a.Campaign END as Skill,

                Reports.dbo.formatIntervals(CASE
                WHEN @AGRUPADO = 0 THEN CONVERT(DATETIME,Reports.dbo.getInterval(StartDate))
                WHEN @AGRUPADO = 1 THEN CONVERT(DATE,DATEADD(HH,-6,StartDate))
                WHEN @AGRUPADO = 2 THEN CONVERT(DATETIME,DATEPART(MONTH,StartDate) - 1)
                ELSE NULL
            END) as Fecha,


                SUM(CASE WHEN Direction='in' AND IsTaked = 1 AND IsGhostCallThresHold = 0 AND OutOfHour = 0 THEN 1 ELSE 0  END) + SUM(CASE WHEN Direction='in' AND IsAbandoned = 1 AND IsGhostCallThresHold = 0 AND OutOfHour = 0 THEN 1 ELSE 0  END) as Recibidas,
                SUM(CASE WHEN Direction='in' AND IsTaked = 1 AND IsGhostCallThresHold = 0 AND OutOfHour = 0 THEN 1 ELSE 0  END) as Atendidas,
                SUM(CASE WHEN Direction='in' AND IsAbandoned = 1 AND IsGhostCallThresHold = 0 AND OutOfHour = 0 THEN 1 ELSE 0  END) as Abandonadas,

                SUM(CASE WHEN Direction='in' AND IsTaked = 1 AND IsGhostCallThresHold = 0 AND OutOfHour = 0 AND AttentionTime <=20 THEN 1 ELSE 0 END) 'Ate<20',
                SUM(CASE WHEN Direction='in' AND IsTaked = 1 AND IsGhostCallThresHold = 0 AND OutOfHour = 0 AND AttentionTime >20 THEN 1 ELSE 0 END) 'Ate>20',

                SUM(CASE WHEN Direction='in' AND IsTaked = 1 AND IsGhostCallThresHold = 0 AND OutOfHour = 0 AND AttentionTime = 1 THEN 1 ELSE 0 END) 'A1',
                SUM(CASE WHEN Direction='in' AND IsTaked = 1 AND IsGhostCallThresHold = 0 AND OutOfHour = 0 AND AttentionTime = 2 THEN 1 ELSE 0 END) 'A2',
                SUM(CASE WHEN Direction='in' AND IsTaked = 1 AND IsGhostCallThresHold = 0 AND OutOfHour = 0 AND AttentionTime = 3 THEN 1 ELSE 0 END) 'A3',
                SUM(CASE WHEN Direction='in' AND IsTaked = 1 AND IsGhostCallThresHold = 0 AND OutOfHour = 0 AND AttentionTime >= 20 THEN 1 ELSE 0 END) 'A4',

                SUM(CASE WHEN Direction='in' AND IsAbandoned = 1 AND IsGhostCallThresHold = 1 AND OutOfHour = 0 THEN 1 ELSE 0  END) 'Aba<5',
                SUM(CASE WHEN Direction='in' AND IsAbandoned = 1 AND IsGhostCallThresHold = 0 AND OutOfHour = 0 AND DesertionTime >5 THEN 1 ELSE 0 END) 'Aba>5',

                SUM(CASE WHEN Direction='out' AND IsGhostCallThresHold = 0 AND OutOfHour = 0 THEN 1 ELSE 0  END) as Salientes,
                SUM(CASE WHEN Direction='out' AND IsTaked = 1 AND IsGhostCallThresHold = 0 AND OutOfHour = 0 THEN 1 ELSE 0  END) as 'Salientes Atendidas',

                Reports.dbo.formatSeconds(ISNULL(SUM(CASE WHEN Direction='in' AND IsGhostCallThresHold = 0 THEN DurationTime ELSE 0  END)/NULLIF(SUM(CASE WHEN Direction='in' AND IsGhostCallThresHold = 0  THEN 1 ELSE 0  END),0),0)) as 'AHT Inbound',
                Reports.dbo.formatSeconds(ISNULL(SUM(CASE WHEN Direction='out' AND IsTaked = 1 AND IsGhostCallThresHold = 0 THEN DurationTime ELSE 0  END)/NULLIF(SUM(CASE WHEN Direction='out' AND IsTaked = 1 AND IsGhostCallThresHold = 0  THEN 1 ELSE 0  END),0),0)) as 'AHT Outbound',

                Reports.dbo.formatSeconds(ISNULL(SUM(CASE WHEN IsTaked = 1 AND Direction='in' AND IsGhostCallThresHold = 0 THEN AttentionTime ELSE 0  END)/NULLIF(SUM(CASE WHEN IsTaked = 1  AND Direction='in' AND IsGhostCallThresHold = 0  THEN 1 ELSE 0  END),0),0)) as 'ATT Inbound',
                Reports.dbo.formatSeconds(ISNULL(SUM(CASE WHEN IsTaked = 1 AND Direction='out' AND IsGhostCallThresHold = 0 THEN AttentionTime ELSE 0  END)/NULLIF(SUM(CASE WHEN IsTaked = 1  AND Direction='out' AND IsGhostCallThresHold = 0  THEN 1 ELSE 0  END),0),0)) as 'ATT Outboound',

                Reports.dbo.formatSeconds(ISNULL(SUM(CASE WHEN IsTaked = 1 THEN RingingTime ELSE 0  END)/NULLIF(SUM(CASE WHEN IsTaked = 1 THEN 1 ELSE 0  END),0),0)) as ASA,
                Reports.dbo.formatSeconds(ISNULL(SUM(CASE WHEN IsAbandoned = 1 THEN AnswerTime ELSE 0  END)/NULLIF(SUM(CASE WHEN IsAbandoned = 1 THEN 1 ELSE 0  END),0),0)) as AAT

            FROM inConcert.dbo.interactions a
                LEFT JOIN Campaigns.dbo.campaigns b ON a.Campaign = b.skill


            WHERE
        ((CASE WHEN @CAMPAIGN = '-1' THEN NULL ELSE @CAMPAIGN END IS NULL) OR (EXISTS (SELECT NULL
                FROM @tmpCampanias
                WHERE Campania COLLATE DATABASE_DEFAULT = b.campaign)))
                AND
                ((CASE WHEN @SKILL = '-1' THEN NULL ELSE @SKILL END IS NULL) OR (EXISTS (SELECT NULL
                FROM @tmpSkills
                WHERE Skill COLLATE DATABASE_DEFAULT = a.Campaign /*and b.tipo <5*/)))
                AND
                CONVERT(DATE,DATEADD(HH,-6,StartDate)) BETWEEN @INT_FECHA_INI AND @INT_FECHA_FIN

            GROUP BY
            CASE WHEN @TOTALIZADO = 1 THEN 'Todos' ELSE b.campaign END,
            CASE WHEN @TOTALIZADO =1 THEN 'Todos' ELSE a.Campaign END,
            CASE
                WHEN @AGRUPADO = 0 THEN Reports.dbo.getInterval(StartDate)
                WHEN @AGRUPADO = 1 THEN CONVERT(DATE,DATEADD(HH,-6,StartDate))
                WHEN @AGRUPADO = 2 THEN DATEPART(MONTH,StartDate) - 1
                ELSE NULL
            END
        END`);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`DROP PROCEDURE campaignAccumulators`);
  }
}
