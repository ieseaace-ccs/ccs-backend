import { MigrationInterface, QueryRunner } from 'typeorm';

export class campaignAccumulatorsMail1633719580178
  implements MigrationInterface
{
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`CREATE PROCEDURE campaignAccumulatorsMail
    --DECLARE
        @INTERVALO INT,
        @AGRUPADO INT,
        @TOTALIZADO INT,
        @CAMPAIGN VARCHAR(MAX) ,
        @SKILL VARCHAR(MAX),
        @FECHA_INI DATETIME = NULL,
        @FECHA_FIN DATETIME = NULL

 /* SET @INTERVALO = 4
    SET @AGRUPADO = 1
    SET @TOTALIZADO = 0
    SET @CAMPAIGN = 'viajespalacio||'
    SET @SKILL = -1
    SET @FECHA_INI = '2021-09-01'
    SET @FECHA_FIN = '2021-10-01' */

    AS BEGIN

        SET DATEFIRST 1

        DECLARE @INT_FECHA_INI DATETIME, @INT_FECHA_FIN DATETIME

        IF @INTERVALO = 0 BEGIN

            SET @INT_FECHA_INI = DATEADD(dd,DATEDIFF(dd,0,GETDATE()),0)
            SET @INT_FECHA_FIN = DATEADD(ms,-3,DATEADD(dd,DATEDIFF(dd,0,GETDATE()),1))

        END

        IF @INTERVALO = 1 BEGIN

            SET @INT_FECHA_INI = DATEADD(wk,DATEDIFF(wk,0,GETDATE()),0)
            SET @INT_FECHA_FIN = DATEADD(wk,DATEDIFF(wk,0,GETDATE()),7)

        END

        IF @INTERVALO = 2 BEGIN

            SET @INT_FECHA_INI = DATEADD(mm,DATEDIFF(mm,0,GETDATE()),0)
            SET @INT_FECHA_FIN = DATEADD(ms,-3,DATEADD(mm,0,DATEADD(mm,DATEDIFF(mm,0,GETDATE())+1,0)))

        END

        IF @INTERVALO = 3 BEGIN

            SET @INT_FECHA_INI = DATEADD(yy,DATEDIFF(yy,0,GETDATE()),0)
            SET @INT_FECHA_FIN = DATEADD(ms,-3,DATEADD(yy,0,DATEADD(yy,DATEDIFF(yy,0,GETDATE())+1,0)))

        END

        IF @INTERVALO = 4 BEGIN

            SET @INT_FECHA_INI = @FECHA_INI
            SET @INT_FECHA_FIN = @FECHA_FIN + 1

        END

        DECLARE @tmpCampanias AS TABLE(Campania VARCHAR(MAX))
        INSERT INTO @tmpCampanias
            (Campania)
        SELECT idCampo
        FROM Reports.dbo.multiSelect(@CAMPAIGN)

        DECLARE @tmpSkills AS TABLE(Skill VARCHAR(MAX))
        INSERT INTO @tmpSkills
            (Skill)
        SELECT idCampo
        FROM Reports.dbo.multiSelect(@SKILL)



        SELECT


            CASE WHEN @TOTALIZADO = 1 THEN 'Todos' ELSE b.campaign END as Campania,
            CASE WHEN @TOTALIZADO =1 THEN 'Todos' ELSE a.Campaign END as Skill,

            Reports.dbo.formatIntervals(CASE
            WHEN @AGRUPADO = 0 THEN CONVERT(DATETIME,Reports.dbo.getInterval(dbo.switchUTCtoLocal(StartDate)))
            WHEN @AGRUPADO = 1 THEN CONVERT(DATE,dbo.switchUTCtoLocal(StartDate))
            WHEN @AGRUPADO = 2 THEN CONVERT(DATETIME,DATEPART(MONTH,dbo.switchUTCtoLocal(StartDate)) - 1)
            ELSE NULL
        END) as Fecha,

            SUM(CASE WHEN mailhasResponse = 1 THEN 1 ELSE 0  END) + SUM(CASE WHEN mailhasResponse = 0 THEN 1 ELSE 0  END) as Recibidas,
            SUM(CASE WHEN mailhasResponse = 1 THEN 1 ELSE 0  END) as Atendidas,
            SUM(CASE WHEN mailhasResponse = 0  THEN 1 ELSE 0  END) as Abandonadas,
            Reports.dbo.formatSeconds(ISNULL(SUM(CASE WHEN 1=1 THEN DurationTime ELSE 0  END)/NULLIF(SUM(CASE WHEN 1=1  THEN 1 ELSE 0  END),0),0)) as AHT,
            Reports.dbo.formatSeconds(ISNULL(SUM(CASE WHEN mailhasResponse = 1 THEN AttentionTime ELSE 0  END)/NULLIF(SUM(CASE WHEN mailhasResponse = 1  THEN 1 ELSE 0  END),0),0)) as ATT,
            Reports.dbo.formatSeconds(ISNULL(SUM(CASE WHEN mailhasResponse = 1 THEN waitForAnswerTime ELSE 0  END)/NULLIF(SUM(CASE WHEN mailhasResponse = 1 THEN 1 ELSE 0  END),0),0)) as ASA,
            Reports.dbo.formatSeconds(ISNULL(SUM(CASE WHEN mailhasResponse = 0 THEN waitForAnswerTime ELSE 0  END)/NULLIF(SUM(CASE WHEN mailhasResponse = 0 THEN 1 ELSE 0  END),0),0)) as AAT


        FROM inConcert.dbo.interactions_mail a
            LEFT JOIN Campaigns.dbo.campaigns b ON a.Campaign = b.skill


        WHERE
    ((CASE WHEN @CAMPAIGN = '-1' THEN NULL ELSE @CAMPAIGN END IS NULL) OR (EXISTS (SELECT NULL
            FROM @tmpCampanias
            WHERE Campania COLLATE DATABASE_DEFAULT = b.campaign)))
            AND
            ((CASE WHEN @SKILL = '-1' THEN NULL ELSE @SKILL END IS NULL) OR (EXISTS (SELECT NULL
            FROM @tmpSkills
            WHERE Skill COLLATE DATABASE_DEFAULT = a.Campaign /*and b.tipo <5*/)))
            AND
            CONVERT(DATE,dbo.switchUTCtoLocal(StartDate)) BETWEEN @INT_FECHA_INI AND @INT_FECHA_FIN

        GROUP BY
        CASE WHEN @TOTALIZADO = 1 THEN 'Todos' ELSE b.campaign END,
        CASE WHEN @TOTALIZADO =1 THEN 'Todos' ELSE a.Campaign END,
        CASE
            WHEN @AGRUPADO = 0 THEN Reports.dbo.getInterval(dbo.switchUTCtoLocal(StartDate))
            WHEN @AGRUPADO = 1 THEN CONVERT(DATE,dbo.switchUTCtoLocal(StartDate))
            WHEN @AGRUPADO = 2 THEN CONVERT(DATETIME,DATEPART(MONTH,dbo.switchUTCtoLocal(StartDate)) - 1)
            ELSE NULL
        END
   END`);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`DROP FUNCTION campaignAccumulatorsMail`);
  }
}
