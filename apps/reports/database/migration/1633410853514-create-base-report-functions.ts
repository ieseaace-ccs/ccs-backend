import { MigrationInterface, QueryRunner } from 'typeorm';

export class createBaseReportFunctions1633410853514
  implements MigrationInterface
{
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`CREATE FUNCTION formatIntervals
    (
        @DATE DATETIME
    )
    RETURNS NVARCHAR(20)
    AS
    BEGIN
        DECLARE @PARSED_DATE NVARCHAR(20)
        SELECT @PARSED_DATE =   CONVERT(VARCHAR(10), @DATE, 103) + ' '  + convert(VARCHAR(8), @DATE, 14)
        RETURN @PARSED_DATE
    END`);

    await queryRunner.query(`CREATE FUNCTION formatSeconds
    (
        @DATE DECIMAL
    )
    RETURNS VARCHAR(8)
    AS
    BEGIN
        DECLARE @PARSED_DATE VARCHAR(8)
        SELECT @PARSED_DATE = CONVERT(varchar, DATEADD(ms, @DATE * 1000, 0), 114)
        RETURN @PARSED_DATE
    END`);

    await queryRunner.query(`CREATE FUNCTION getInterval
    (
        @DATE DATETIME
    )
    RETURNS DATETIME
    AS
    BEGIN
        DECLARE @PARSED_DATE DATETIME
        SELECT @PARSED_DATE = DATETIMEFROMPARTS(DATEPART(YEAR,@DATE),DATEPART(MONTH,@DATE),DATEPART(DAY,@DATE), DATEPART(HOUR,@DATE),CASE WHEN DATEPART(MINUTE,@DATE) <30 THEN 0 ELSE 30 END,0,0)
        RETURN @PARSED_DATE
    END`);

    await queryRunner.query(`CREATE FUNCTION multiSelect
    (
        @strValue VARCHAR(4096)
    )
    RETURNS @tmpTable TABLE(idCampo VARCHAR(100))
    AS 
    BEGIN
            WHILE(PATINDEX('%||%', @strValue)!=0)
            BEGIN
                INSERT INTO @tmpTable VALUES (LEFT(@strValue, PATINDEX('%||%', @strValue)-1))
                SET @strValue=RIGHT(@strValue, LEN(@strValue)-PATINDEX('%||%', @strValue)-1)	
            END;
        RETURN;
    END;`);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`DROP FUNCTION formatIntervals`);
    await queryRunner.query(`DROP FUNCTION formatSeconds`);
    await queryRunner.query(`DROP FUNCTION getInterval`);
    await queryRunner.query(`DROP FUNCTION multiSelect`);
  }
}
