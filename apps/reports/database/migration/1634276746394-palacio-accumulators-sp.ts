import { MigrationInterface, QueryRunner } from 'typeorm';

export class palacioAccumulatorsSp1634276746394 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`CREATE PROCEDURE viajesPalacioAccumulators 
                @INTERVALO INT,
                @AGRUPADO INT,
                @FECHA_INI DATETIME = NULL,
                @FECHA_FIN DATETIME = NULL

            --SET @INTERVALO = 4
            --SET @AGRUPADO = 1
            --SET @FECHA_INI = '2021-10-01'
            --SET @FECHA_FIN = '2021-10-31'

            AS
            BEGIN

            SET DATEFIRST 1

            DECLARE @INT_FECHA_INI DATETIME, @INT_FECHA_FIN DATETIME
            IF @INTERVALO = 0
            BEGIN

            SET @INT_FECHA_INI = DATEADD(dd, DATEDIFF(dd, 0, GETDATE()), 0)
            SET @INT_FECHA_FIN = DATEADD(ms, -3, DATEADD(dd, DATEDIFF(dd, 0, GETDATE()), 1))


            END
            IF @INTERVALO = 1
            BEGIN

            SET @INT_FECHA_INI = DATEADD(wk, DATEDIFF(wk, 0, GETDATE() - 1), 0)
            SET @INT_FECHA_FIN = DATEADD(wk, DATEDIFF(wk, 0, GETDATE() - 1), 7)

            END
            IF @INTERVALO = 2
            BEGIN

            SET @INT_FECHA_INI = DATEADD(mm, DATEDIFF(mm, 0, GETDATE()), 0)
            SET @INT_FECHA_FIN = DATEADD(ms, -3, DATEADD(mm, 0, DATEADD(mm, DATEDIFF(mm, 0, GETDATE()) + 1, 0)))

            END
            IF @INTERVALO = 3
            BEGIN

            SET @INT_FECHA_INI = DATEADD(yy, DATEDIFF(yy, 0, GETDATE()), 0)
            SET @INT_FECHA_FIN = DATEADD(ms, -3, DATEADD(yy, 0, DATEADD(yy, DATEDIFF(yy, 0, GETDATE()) + 1, 0)))

            END
            IF @INTERVALO = 4
            BEGIN

            SET @INT_FECHA_INI = @FECHA_INI
            SET @INT_FECHA_FIN = @FECHA_FIN + 1

            END


            SELECT Reports.dbo.formatIntervals(CASE
                WHEN @AGRUPADO = 0 THEN CONVERT(DATETIME, Reports.dbo.getInterval(date))
                WHEN @AGRUPADO = 1 THEN CONVERT(DATE, date)
                WHEN @AGRUPADO = 2 THEN CONVERT(DATETIME, DATEPART(MONTH, date) - 1)
                ELSE NULL
            END)                                     as fecha,
            COUNT(*)                              as capturadas,
            SUM(iif(pos_records_total = 0, 1, 0)) as vendidas,
            SUM(total_local_price)                as monto_capturado,
            SUM(pos_records_total)                as monto_vendido,
            SUM(total_local_price) / COUNT(*)     as ticket_promedio
            FROM inConcert.dbo.viajes_palacio a


            WHERE CONVERT(DATE, date) BETWEEN @INT_FECHA_INI AND @INT_FECHA_FIN

            GROUP BY CASE
            WHEN @AGRUPADO = 0 THEN Reports.dbo.getInterval(date)
            WHEN @AGRUPADO = 1 THEN CONVERT(DATE, date)
            WHEN @AGRUPADO = 2 THEN DATEPART(MONTH, date) - 1
            ELSE NULL
            END
            END`);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`DROP PROCEDURE viajesPalacioAccumulators`);
  }
}
