import { MigrationInterface, QueryRunner } from 'typeorm';

export class createObjetivesTable1634782154248 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`CREATE TABLE dbo.objetives
        (
            uuid uniqueidentifier constraint DF_951b8f1dfc94ac1d0301a14b7e default newsequentialid() not null constraint PK_951b8f1dfc94ac1d0301a14b7e2 primary key,
            campaign varchar(max) not null,
            name varchar(max) not null,
            value int not null
        )`);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`DROP TABLE dbo.objetives`);
  }
}
