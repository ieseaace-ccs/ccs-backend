import * as Joi from 'joi';

export const configSchema = Joi.object({
  SENTRY_DSN: Joi.string().required(),
  REPORTS_MICROSERVICE_HOST: Joi.string().default('localhost'),
  REPORTS_MICROSERVICE_PORT: Joi.number().required(),
  REPORTS_DB_TYPE: Joi.string().required(),
  REPORTS_DB_HOST: Joi.string().required(),
  REPORTS_DB_PORT: Joi.number().required(),
  REPORTS_DB_USERNAME: Joi.string().required(),
  REPORTS_DB_PASSWORD: Joi.string(),
  REPORTS_DB_NAME: Joi.string().required(),
  REPORTS_DB_RUN_MIGRATIONS: Joi.number(),
  REPORTS_DB_SYNC: Joi.number(),
  REPORTS_DB_TRUST_CERTIFICATE: Joi.number().required(),
  AUTH_MICROSERVICE_HOST: Joi.string().default('localhost'),
  AUTH_MICROSERVICE_PORT: Joi.number().required()
});
