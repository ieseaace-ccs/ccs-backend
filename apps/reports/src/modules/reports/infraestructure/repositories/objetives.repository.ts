import { RpcException } from '@nestjs/microservices';
import { Repository, EntityRepository } from 'typeorm';
import { ObjetivesEntity } from '../../domain/models/entities/objetives.entity';

@EntityRepository(ObjetivesEntity)
export class ObjetivesRepository extends Repository<ObjetivesEntity> {
  getObjetive(campaign: string, kpi: string): Promise<ObjetivesEntity> {
    return this.findOne({ where: { campaign, name: kpi } });
  }
  async setObjetive(
    campaign: string,
    kpi: string,
    value: number
  ): Promise<ObjetivesEntity> {
    const KPI = await this.findOne({ where: { campaign, name: kpi } });

    if (!KPI) {
      throw new RpcException({ message: 'Not Found', statusCode: '404' });
    }
    return this.save({ ...KPI, value });
  }
}
