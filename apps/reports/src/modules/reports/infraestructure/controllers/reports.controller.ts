import { Controller } from '@nestjs/common';
import { MessagePattern } from '@nestjs/microservices';
import { ReportsService } from '../../application/reports.service';
import { ObjetivesEntity } from '../../domain/models/entities/objetives.entity';

@Controller()
export class ReportsController {
  constructor(private readonly reportsService: ReportsService) {}
  @MessagePattern({ role: 'reports', reports: 'campaignAccumulators' })
  campaignAccumulators(params): Promise<any> {
    return this.reportsService.campaignAcumulators(params);
  }
  @MessagePattern({ role: 'reports', reports: 'palacioAccumulators' })
  palacioAccumulators(params): Promise<any> {
    return this.reportsService.palacioAccumulators(params);
  }

  @MessagePattern({ role: 'reports', reports: 'palacioConversion' })
  palacioConversions(params): Promise<any> {
    return this.reportsService.palacioConversion(params);
  }
  @MessagePattern({ role: 'reports', reports: 'getObjetives' })
  getObjetives(params): Promise<ObjetivesEntity> {
    return this.reportsService.getObjetives(params);
  }
  @MessagePattern({ role: 'reports', reports: 'setObjetives' })
  setObjetives(params): Promise<ObjetivesEntity> {
    return this.reportsService.setObjetives(params);
  }
}
