import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ReportsService } from './application/reports.service';
import { ReportsController } from './infraestructure/controllers/reports.controller';
import { ObjetivesRepository } from './infraestructure/repositories/objetives.repository';

@Module({
  imports: [TypeOrmModule.forFeature([ObjetivesRepository])],
  controllers: [ReportsController],
  providers: [ReportsService]
})
export class ReportsModule {}
