import { Entity, Column, PrimaryGeneratedColumn, BaseEntity } from 'typeorm';

@Entity({ name: 'objetives' })
export class ObjetivesEntity extends BaseEntity {
  @PrimaryGeneratedColumn('uuid')
  uuid: string;

  @Column()
  campaign: string;

  @Column()
  name: string;

  @Column()
  value: number;
}
