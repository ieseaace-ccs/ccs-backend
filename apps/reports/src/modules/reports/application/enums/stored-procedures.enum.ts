export enum StoredProcedures {
  campaignAccumulators = 'campaignAccumulators',
  campaignAccumulatorsChat = 'campaignAccumulatorsChat',
  campaignAccumulatorsWhats = 'campaignAccumulatorsWhats',
  campaignAccumulatorsMail = 'campaignAccumulatorsMail',
  palacioAccumulators = 'viajesPalacioAccumulators',
  palacioConversion = 'viajesPalacioConversion'
}
