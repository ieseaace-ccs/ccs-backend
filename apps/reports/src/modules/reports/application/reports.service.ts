import { Injectable, Logger } from '@nestjs/common';
import { RpcException } from '@nestjs/microservices';
import { InjectRepository } from '@nestjs/typeorm';
import { Connection } from 'typeorm';

import { ObjetivesEntity } from '../domain/models/entities/objetives.entity';
import { ObjetivesRepository } from '../infraestructure/repositories/objetives.repository';
import { StoredProcedures } from './enums/stored-procedures.enum';
import { channelProcedures } from './maps/channel-procedure.map';

@Injectable()
export class ReportsService {
  constructor(
    private readonly connection: Connection,
    @InjectRepository(ObjetivesRepository)
    private readonly objetivesRepo: ObjetivesRepository,
  ) {}

  public async campaignAcumulators({
    interval,
    groupBy,
    totalized,
    campaign,
    skill,
    startDate,
    endDate,
    channel,
  }): Promise<any> {
    const params = [interval, groupBy, totalized, campaign, skill, startDate, endDate];

    const accumulator = this.getChannelProcedure(channel);

    return this.spExecutor(accumulator, params);
  }
  public async palacioAccumulators({ interval, groupBy, startDate, endDate }): Promise<any> {
    const params = [interval, groupBy, startDate, endDate];

    return this.spExecutor(StoredProcedures.palacioAccumulators, params);
  }
  public async palacioConversion({ interval, groupBy, startDate, endDate }): Promise<any> {
    const params = [interval, groupBy, startDate, endDate];

    return this.spExecutor(StoredProcedures.palacioConversion, params);
  }

  public getObjetives({ campaign, indicator }): Promise<ObjetivesEntity> {
    return this.objetivesRepo.getObjetive(campaign, indicator);
  }
  public setObjetives({ campaign, indicator, value }): Promise<ObjetivesEntity> {
    return this.objetivesRepo.setObjetive(campaign, indicator, value);
  }

  private async spExecutor(procedure: StoredProcedures, orderedParams: string[]): Promise<any> {
    try {
      let paramsString = '';
      for (const i in orderedParams) {
        paramsString = `${paramsString} @${i},`;
      }
      const statement = `EXEC ${procedure} ${paramsString}`;
      return await this.connection.query(statement.slice(0, -1), orderedParams);
    } catch (error) {
      Logger.error(error);
      throw new RpcException({
        message: error.message,
        statusCode: '400',
      });
    }
  }

  private getChannelProcedure(channel: string): StoredProcedures {
    return channelProcedures[channel];
  }
}
