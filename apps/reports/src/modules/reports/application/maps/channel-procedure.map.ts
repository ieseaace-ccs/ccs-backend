import { StoredProcedures } from '../enums/stored-procedures.enum';

export const channelProcedures = {
  phone: StoredProcedures.campaignAccumulators,
  chat: StoredProcedures.campaignAccumulatorsChat,
  whatsapp: StoredProcedures.campaignAccumulatorsWhats,
  mail: StoredProcedures.campaignAccumulatorsMail
};
