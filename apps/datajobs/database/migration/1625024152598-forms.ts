import { MigrationInterface, QueryRunner } from 'typeorm';

export class forms1625024152598 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`CREATE TABLE forms (
          interaction varchar(36) not null,
          attribute varchar(255) not null,
          value varchar(900),
          constraint forms_pk
            primary key clustered (interaction, attribute, value)
        )`);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`DROP TABLE dbo.forms`);
  }
}
