import { MigrationInterface, QueryRunner } from 'typeorm';

export class createInteractionsMailTable1633317556055
  implements MigrationInterface
{
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`CREATE TABLE interactions_mail
    (
        id nvarchar(255) not null constraint PK_7a0a7aaa59c4f20f95f7c28af3b primary key,
        campaign nvarchar(255),
        startDate datetime,
        account nvarchar(255),
        accountId nvarchar(255),
        lastAttentionLevel nvarchar(255),
        initiative nvarchar(255),
        parentInteractionType nvarchar(255),
        firstAgent nvarchar(255),
        contactName nvarchar(255),
        isIvr bit,
        pureIvr bit,
        mailHasResponse bit,
        mailResponseDate datetime,
        mailTotalMessages float,
        mailAgentResponseCount float,
        mailBotResponseCount float,
        startAttention datetime,
        endDate datetime,
        durationTime float,
        ivrTime float,
        waitingTime float,
        acdTime float,
        waitForAnswerTime float,
        attentionTime float,
        dispositionCode nvarchar(255),
        dispositionTreePath nvarchar(255),
        dispositionIsGoal bit,
        mailFrom varchar(MAX),
        mailTo varchar(MAX),
        mailCc varchar(MAX),
        mailSubject varchar(MAX),
        mailAttachCount float,
        wasSpam bit,
        mailBatchId nvarchar(255),
        mailBatchMessageSubject nvarchar(255),
        isTransferred nvarchar(255),
        transferSuccessful bit,
        transferDestinationType nvarchar(255),
        transferDestination nvarchar(255),
        hasCci bit,
        ticket nvarchar(255),
        folderFrom nvarchar(255),
        folderTo nvarchar(255),
        vcc nvarchar(255),
        exportContent nvarchar(255),
        isDataFromNotifier bit,
        tmStmp datetime
    )`);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`DROP TABLE dbo.interactions_mail`);
  }
}
