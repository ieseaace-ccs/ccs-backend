import { MigrationInterface, QueryRunner } from 'typeorm';

export class createInteractionsWhatsappTable1633311893382
  implements MigrationInterface
{
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`CREATE TABLE interactions_whats
    (
        id nvarchar(255) not null constraint PK_19fd451c57a7afa888017442717 primary key,
        campaign nvarchar(255),
        startDate datetime,
        account nvarchar(255),
        accountId nvarchar(255),
        lastAttentionLevel nvarchar(255),
        initiative nvarchar(255),
        firstAgent nvarchar(255),
        contactName nvarchar(255),
        isIvr bit,
        pureIvr bit,
        hasResponse bit,
        firstResponseDate datetime,
        whatsappTotalMessages nvarchar(255),
        whatsappTotalClientMessages nvarchar(255),
        whatsappTotalAgentMessages nvarchar(255),
        whatsappTotalBotMessages nvarchar(255),
        whatsappTotalHsmMessages nvarchar(255),
        startAttention datetime,
        endDate datetime,
        durationTime float,
        ivrTime float,
        waitingTime float,
        acdTime float,
        waitForAnswerTime float,
        attentionTime float,
        dispositionCode nvarchar(255),
        dispositionTreePath nvarchar(255),
        dispositionIsGoal bit,
        contactAddress nvarchar(255),
        whatsappPostId nvarchar(255),
        whatsappPostMessage nvarchar(255),
        whatsappInitialMessageId nvarchar(255),
        whatsappTypeOfInitialMessage nvarchar(255),
        whatsappTextOfInitialMessage nvarchar(255),
        isTransferred nvarchar(255),
        transferSuccessful nvarchar(255),
        transferDestinationType nvarchar(255),
        transferDestination nvarchar(255),
        hasCci bit,
        ticket nvarchar(255),
        vcc nvarchar(255),
        exportContent nvarchar(255),
        isDataFromNotifier bit,
        tmStmp datetime
    )`);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`DROP TABLE dbo.interactions_whats`);
  }
}
