import { MigrationInterface, QueryRunner } from 'typeorm';

export class whatsappTextLength1633638901114 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`
    ALTER TABLE interactions_whats 
    ALTER COLUMN whatsappTextOfInitialMessage varchar(max);`);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`
    ALTER TABLE interactions_whats 
    ALTER COLUMN whatsappTextOfInitialMessage nvarchar(255);`);
  }
}
