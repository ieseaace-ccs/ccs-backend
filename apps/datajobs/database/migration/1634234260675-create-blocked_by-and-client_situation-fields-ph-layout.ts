import { MigrationInterface, QueryRunner } from 'typeorm';

export class createBlockedByAndClientSituationFieldsPhLayout1634234260675
  implements MigrationInterface
{
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`
        ALTER TABLE viajes_palacio 
        ADD blocked_by varchar(255),client_situation varchar(255)`);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`
    ALTER TABLE viajes_palacio 
    DROP COLUMN blocked_by,client_situation`);
  }
}
