import { MigrationInterface, QueryRunner } from 'typeorm';

export class agents1623357768910 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`CREATE TABLE agents (
      Actor nvarchar(255) not null,
      VirtualCC nvarchar(255),
      Date datetime not null,
      TotalBreaks int,
      TotalToilettes int,
      TotalTrainings int,
      TotalPersonals int,
      TotalCustoms int,
      TotalPauses int,
      TotalLogins int,
      LoggedState decimal,
      ActiveState decimal,
      BreakState decimal,
      PauseState decimal,
      AttePauseState decimal,
      WrapupPauseState decimal,
      TrainingState decimal,
      PersonalState decimal,
      ToiletteState decimal,
      CustomsState decimal,
      PreferredCampaign nvarchar(255) not null,
      TotalUnavailable decimal,
      UnavailableState decimal,
      AtteActiveState decimal,
      WrapupActiveState decimal,
      DurationCount int,
      DurationTime decimal,
      AnswerCount int,
      AnswerTime decimal,
      WaitCount int,
      WaitTime decimal,
      ControlAgentCount int,
      ControlAgentTime decimal,
      AttentionCount int,
      AttentionTime decimal,
      WrapupCount int,
      WrapupTime decimal,
      DesertionCount int,
      DesertionTime decimal,
      HoldCount int,
      HoldTime decimal,
      TransferCount int,
      TransferTime decimal,
      RequeuedCount int,
      RequeuedTime decimal,
      RingingCount int,
      RingingTime decimal,
      RingBackCount int,
      RingBackTime decimal,
      PreviewCount int,
      PreviewTime decimal,
      constraint agents_pk
        primary key nonclustered (Actor, Date, PreferredCampaign)
    )`);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`DROP TABLE dbo.agents`);
  }
}
