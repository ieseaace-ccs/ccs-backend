import { MigrationInterface, QueryRunner } from 'typeorm';

export class addDispositionTreePath1636332865118 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`
            ALTER TABLE interactions 
            ADD DispositionTreePath varchar(MAX)`);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`
        ALTER TABLE interactions 
        DROP COLUMN DispositionTreePath`);
  }
}
