import { MigrationInterface, QueryRunner } from 'typeorm';

export class createInteractionsChatTable1633293517340
  implements MigrationInterface
{
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`CREATE TABLE interactions_chat
    (
        id nvarchar(255) not null constraint PK_af23b3e7f773559bde7436c90dc primary key,
        vcc nvarchar(255),
        campaign nvarchar(255),
        startDate datetime,
        account nvarchar(255),
        accountId nvarchar(255),
        lastAttentionLevel nvarchar(255),
        contactName nvarchar(255),
        firstTakeAgent nvarchar(255),
        lastTakeAgent nvarchar(255),
        isIvr bit,
        pureIvr bit,
        isTaken bit,
        isFinished bit,
        isAbandoned bit,
        slPositive bit,
        hasAnswerFailures bit,
        isGhost bit,
        isOutofScheduler bit,
        ender nvarchar(255),
        startAttention datetime,
        endDate datetime,
        durationTime float,
        ivrTime float,
        waitingTime float,
        acdTime float,
        waitForAnswerTime float,
        attentionTime float,
        wrapupTime float,
        desertionTime float,
        dispositionCode nvarchar(255),
        dispositionTreePath nvarchar(255),
        dispositionIsGoal bit,
        isTransferred bit,
        transferSuccessful nvarchar(255),
        transferDestinationType nvarchar(255),
        transferDestination nvarchar(255),
        chatMessages nvarchar(255),
        chatClientMessages nvarchar(255),
        chatAgentMessages nvarchar(255),
        chatBotMessages nvarchar(255),
        hasLogin bit,
        hasPreSurvey bit,
        hasPostSurvey bit,
        hasCci bit,
        ticket nvarchar(255),
        exportContent nvarchar(255),
        startWrapupTime datetime,
        isDataFromNotifier bit,
        startTakenTime datetime,
        tmStmp datetime
    )`);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`DROP TABLE dbo.interactions_chat`);
  }
}
