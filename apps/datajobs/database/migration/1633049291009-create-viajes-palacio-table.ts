import { MigrationInterface, QueryRunner } from 'typeorm';

export class createViajesPalacioTable1633049291009
  implements MigrationInterface
{
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`CREATE TABLE dbo.viajes_palacio
        (
          identifier int not null constraint PK_183c360f7a9c66f361b048e048b primary key,
          client nvarchar(255),
          date datetime,
          expiration datetime,
          notice nvarchar(255),
          agent_name nvarchar(255),
          office nvarchar(255),
          status nvarchar(255),
          price int,
          total_price int,
          total_local_price int,
          pos_records_total int,
          month nvarchar(255),
          year int,
          expiration_month nvarchar(255),
          expiration_year nvarchar(255),
          confirmation_date datetime,
          confirmation_month nvarchar(255),
          confirmation_year int,
          confirmation_agent nvarchar(255),
          requested_by nvarchar(255),
          prospect_agent nvarchar(255),
          trip_reason nvarchar(255),
          destination nvarchar(255),
          channel nvarchar(255),
          start_date datetime,
          passengers int,
          region nvarchar(255),
          city nvarchar(255),
          suburb nvarchar(255),
          created_at datetime default getdate() not null,
          updated_at datetime default getdate() not null,
          created_by nvarchar(255)
        )`);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`DROP TABLE dbo.viajes_palacio`);
  }
}
