import {
  BaseEntity,
  Column,
  Entity,
  PrimaryColumn,
  CreateDateColumn,
  UpdateDateColumn,
} from 'typeorm';

@Entity('viajes_palacio_layout', { schema: 'dbo' })
export class ViajesPalacioEntity extends BaseEntity {
  @PrimaryColumn('int')
  identifier: number;

  @PrimaryColumn('datetime')
  date: Date | null;

  @Column('nvarchar', { nullable: true, length: 255 })
  client: string | null;

  @Column('nvarchar', { nullable: true, length: 255 })
  agent: string | null;

  @Column('nvarchar', { nullable: true, length: 255 })
  office: string | null;

  @Column('int', { nullable: true })
  quantity: number | null;

  @PrimaryColumn('nvarchar')
  currency: string | null;

  @Column('float', { nullable: true })
  pos: number | null;
  @Column('float', { nullable: true })
  price: number | null;
  @Column('float', { nullable: true })
  cost: number | null;
  @Column('float', { nullable: true })
  billed: number | null;
  @Column('float', { nullable: true })
  remited: number | null;
  @Column('float', { nullable: true })
  earned: number | null;
  @Column('float', { nullable: true })
  received: number | null;
  @Column('float', { nullable: true })
  to_earn: number | null;
  @Column('float', { nullable: true })
  to_receive: number | null;
  @Column('float', { nullable: true })
  paid: number | null;
  @Column('float', { nullable: true })
  to_pay: number | null;
  @Column('float', { nullable: true })
  exchange_rate: number | null;
  @Column('float', { nullable: true })
  pos_local: number | null;
  @Column('float', { nullable: true })
  earned_local: number | null;
  @Column('float', { nullable: true })
  to_earn_local: number | null;

  @CreateDateColumn({
    name: 'created_at',
  })
  createdAt: Date;

  @UpdateDateColumn({
    name: 'updated_at',
  })
  updatedAt: Date;

  @Column('nvarchar', { nullable: true, length: 255 })
  created_by: string | null;
}
