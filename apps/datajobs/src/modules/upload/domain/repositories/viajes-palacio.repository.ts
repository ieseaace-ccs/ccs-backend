import { Repository, EntityRepository } from 'typeorm';

import { ViajesPalacioEntity } from '../models/entities/viajes-palacio.entity';

@EntityRepository(ViajesPalacioEntity)
export class ViajesPalacioRepository extends Repository<ViajesPalacioEntity> {}
