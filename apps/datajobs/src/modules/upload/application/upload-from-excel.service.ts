import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { AwsS3Service } from '@app/aws-s3';

import { viajesPalacioMap } from '../domain/maps/viajes-palacio.map';
import { viajesPalacioSchema } from '../infrastructure/schemas/viajes-palacio.schema';
import { FileFormatter } from './file-formatter.service';
import { ViajesPalacioEntity } from '../domain/models/entities/viajes-palacio.entity';
import { ViajesPalacioRepository } from '../domain/repositories/viajes-palacio.repository';
import { UploadResponseDto } from '../domain/models/dtos/upload-response.dto';
import nullish from '../domain/constants/custom-nullish';
import { AwsConfigType } from '../../config/infrastructure/types/aws.type';
import { ApplicationConfigType } from '../../config/infrastructure/types/application.type';
import { ConfigService } from '@nestjs/config';
import { CurrencyConverter } from '../infrastructure/services/currency-converter';

@Injectable()
export class UploadService {
  public headers = Object.keys(viajesPalacioMap);
  public config: AwsConfigType;
  public app: ApplicationConfigType;
  constructor(
    @InjectRepository(ViajesPalacioEntity)
    private readonly viajesPalacioRepository: ViajesPalacioRepository,
    private readonly awsS3: AwsS3Service,
    private readonly configService: ConfigService,
    private readonly currencyConverter: CurrencyConverter,
  ) {
    this.config = this.configService.get<AwsConfigType>('aws');
    this.app = this.configService.get<ApplicationConfigType>('application');
  }
  public async uploadLayout(file: any, user: any): Promise<UploadResponseDto> {
    const { headers, config, app } = this;
    const { accesKey, secretKey, bucket } = config;
    const { maxUploadRecords } = app;
    const { username } = user;
    file.buffer = Buffer.from(file.buffer.data);

    const json = FileFormatter.parseExcelBufferToJson(file);
    const validatedHeaders = FileFormatter.validateHeaders(json, headers);
    const validatedJson = FileFormatter.normalizeHeadersAndData(
      validatedHeaders,
      viajesPalacioMap,
      username,
      nullish,
    );

    const exchangeRate = await this.currencyConverter.getRate('USD', 'MXN');
    const enrichedData = FileFormatter.convertCurrency(validatedJson, exchangeRate);

    const dataToSave = FileFormatter.validateData(
      enrichedData,
      viajesPalacioSchema,
      maxUploadRecords,
    );

    await this.awsS3
      .setCredentials(accesKey, secretKey)
      .uploadFromBuffer(file, 'viajes-palacio-layouts', username, bucket);

    const uploaded = await this.viajesPalacioRepository.save(dataToSave, {
      chunk: 10,
    });

    return { records_uploaded: uploaded.length };
  }
}
