import { HttpStatus } from '@nestjs/common';
import { RpcException } from '@nestjs/microservices';
import * as XLSX from 'xlsx';

export class FileFormatter {
  public static parseExcelBufferToJson(file: any): any[] {
    const excel = XLSX.read(file.buffer, {
      type: 'buffer',
      cellDates: true,
    });
    const sheets = excel.SheetNames;
    if (sheets.length > 1) {
      throw new RpcException({
        message: 'The workbook has more than one sheet',
        statusCode: HttpStatus.BAD_REQUEST,
      });
    }

    return XLSX.utils.sheet_to_json(excel.Sheets[sheets[0]], {
      defval: null,
    });
  }
  public static validateHeaders(data: any[], headers: string[]) {
    const headerNames = Object.keys(data[0]);

    if (headerNames.length !== headers.length) {
      throw new RpcException({
        message: 'Diferent header length',
        statusCode: HttpStatus.BAD_REQUEST,
      });
    }

    const validHeaders = headerNames.filter((header) => headers.includes(header));
    const invalidHeaders = headerNames.filter((header) => !headers.includes(header));

    if (validHeaders.length !== headers.length) {
      throw new RpcException({
        message: `Some headers are wrong`,
        statusCode: HttpStatus.BAD_REQUEST,
        detail: invalidHeaders,
      });
    }

    return data;
  }

  public static normalizeHeadersAndData(
    data: any[],
    map: Record<string, any>,
    user: string,
    customNullish: string[] = [],
  ) {
    return data.map((record) => {
      record.created_by = user;
      return Object.keys(record)
        .map((key) => {
          const newKey = map[key] || key;
          return {
            [newKey]: customNullish.includes(record[key]) ? null : record[key],
          };
        })
        .reduce((a, b) => Object.assign({}, a, b));
    });
  }

  public static convertCurrency(data: any[], rate: number) {
    return data.map((record) => {
      if (record.currency === 'MXN') {
        record.exchange_rate = 0;
        record.pos_local = record.pos;
        record.earned_local = record.earned;
        record.to_earn_local = record.to_earn;
      } else {
        if (!record.currency) record.currency = 'UKW';
        record.exchange_rate = rate;
        record.pos_local = record.pos * record.exchange_rate;
        record.earned_local = record.earned * record.exchange_rate;
        record.to_earn_local = record.to_earn * record.exchange_rate;
      }

      return record;
    });
  }

  public static validateData(arrayData: any[], schema: any, maxImportLength: number) {
    const valids = [];
    const invalids = [];

    if (arrayData.length >= maxImportLength) {
      throw new RpcException({
        message: `The file must have less than ${maxImportLength} records`,
        statusCode: HttpStatus.BAD_REQUEST,
      });
    }

    arrayData.forEach((jsonData) => {
      const { error, value } = schema.validate(jsonData, {
        abortEarly: false,
      });

      if (!error) {
        valids.push(value);
      } else {
        const rowErrors = error.details.map((errorObject) => {
          return {
            field: errorObject.context.label,
            value: errorObject.context.value,
            message: errorObject.message,
          };
        });

        invalids.push({ identifier: jsonData.identifier, rowErrors });
      }
    });

    //console.log(invalids);
    if (invalids.length > 0) {
      throw new RpcException({
        message: 'Some fields are invalid',
        detail: invalids,
        statusCode: HttpStatus.BAD_REQUEST,
      });
    }
    return valids;
  }
}
