import { HttpModule, Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AwsS3Module } from '@app/aws-s3';
import { UploadService } from './application/upload-from-excel.service';
import { ViajesPalacioEntity } from './domain/models/entities/viajes-palacio.entity';
import { UploadController } from './infrastructure/controllers/upload.controller';
import { CurrencyConverter } from './infrastructure/services/currency-converter';

@Module({
  imports: [TypeOrmModule.forFeature([ViajesPalacioEntity]), AwsS3Module, HttpModule],
  providers: [UploadService, CurrencyConverter],
  controllers: [UploadController],
})
export class UploadModule {}
