import { HttpService, Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';

@Injectable()
export class CurrencyConverter {
  constructor(private readonly http: HttpService, private readonly config: ConfigService) {}

  public async getRate(from: string, to: string): Promise<number> {
    const result = await this.http
      .get(`${this.config.get('currency').url}/convert?to=${to}&from=${from}&amount=1`, {
        headers: { apikey: this.config.get('currency').key },
      })
      .toPromise();

    return result.data.info.rate;
  }
}
