import { Controller } from '@nestjs/common';
import { MessagePattern } from '@nestjs/microservices';

import { UploadService } from '../../application/upload-from-excel.service';
import { UploadResponseDto } from '../../domain/models/dtos/upload-response.dto';

@Controller()
export class UploadController {
  constructor(private readonly uploadService: UploadService) {}

  @MessagePattern({ role: 'file', cmd: 'upload' })
  getUser({ file, user }): Promise<UploadResponseDto> {
    return this.uploadService.uploadLayout(file, user);
  }
}
