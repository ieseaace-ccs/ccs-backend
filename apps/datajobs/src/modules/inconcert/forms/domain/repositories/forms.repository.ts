import { Repository, EntityRepository } from 'typeorm';

import { FormOutputDto } from '../models/dtos/form-output.dto';
import { FormsEntity } from '../models/entities/form.entity';

@EntityRepository(FormsEntity)
export class FormsRepository extends Repository<FormsEntity> {
  public async saveForms(forms: FormOutputDto[]): Promise<FormOutputDto[]> {
    return this.save(forms, { chunk: 500 });
  }
}
