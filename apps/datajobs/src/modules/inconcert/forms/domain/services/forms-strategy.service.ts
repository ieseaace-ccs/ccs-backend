import { ConfigService } from '@nestjs/config';
import { Injectable, CACHE_MANAGER, Inject } from '@nestjs/common';
import { Cache } from 'cache-manager';
import { InjectRepository } from '@nestjs/typeorm';
import { DateTime } from 'luxon';

import { ApplicationConfigType } from './../../../../config/infrastructure/types/application.type';
import { PersistorStrategy } from '../../../shared/domain/models/interfaces/inconcert-strategy.interface';
import { FormDto } from '../models/dtos/form-input.dto';
import { FormOutputDto } from '../models/dtos/form-output.dto';
import { InconcertApiService } from '../../../shared/infrastructure/services/inconcert.service';
import { FormsRepository } from '../repositories/forms.repository';
import { DataPersistenceService } from '../../../shared/domain/services/data-persistence.service';
import { Channels } from '../../../shared/infrastructure/enums/channels.enum';

@Injectable()
export class FormsStrategy implements PersistorStrategy {
  private readonly strategyName = 'Forms';
  private readonly config: ApplicationConfigType;
  constructor(
    @InjectRepository(FormsRepository)
    private readonly formsRepository: FormsRepository,
    private readonly inconcertApiService: InconcertApiService,
    private readonly configService: ConfigService,
    private readonly dataPersistenceService: DataPersistenceService,
    @Inject(CACHE_MANAGER) private readonly cacheManager: Cache,
  ) {
    this.config = this.configService.get<ApplicationConfigType>('application');
  }

  public getStrategyName() {
    return this.strategyName;
  }

  public async fetch(date: string, channel: Channels = Channels.PHONE): Promise<FormDto[]> {
    const { campaigns } = this.config;

    const fetchDate = await this.dataPersistenceService.getFetchDate(date, this.getStrategyName());
    return campaigns.reduce(async (prev, actual) => {
      const fetchResult = await this.inconcertApiService.fetchForms(fetchDate, actual, channel);

      const transposedResult = this.transpose(fetchResult);

      return [...(await prev), ...transposedResult];
    }, Promise.resolve([]));
  }

  public async format(formsArray: FormDto[]): Promise<FormOutputDto[]> {
    const formatedArray = formsArray.map((form) => {
      return {
        interaction: form.id,
        attribute: form.attribute,
        value: String(form.value).substr(0, 850),
      };
    });

    const filteredFromCache = await this.filterCachedRegistry(formatedArray);

    await this.setFormsCache(formatedArray);

    return filteredFromCache;
  }

  public async save(formDto: FormOutputDto[]): Promise<FormOutputDto[]> {
    try {
      return await this.formsRepository.saveForms(formDto);
    } catch (error) {
      await this.cacheManager.del(this.getStrategyName());
      throw error;
    }
  }

  private transpose(formsArray: any[]): any[] {
    const { excludedFormFields } = this.config;
    return formsArray
      .reduce((prevRegister, actualRegister) => {
        const newRegister = Object.keys(actualRegister).map((key) => ({
          id: this.removeBrackets(actualRegister.Id),
          attribute: key,
          value: actualRegister[key],
        }));

        return [...prevRegister, ...newRegister];
      }, [])
      .filter(
        (registerToFilter) =>
          !excludedFormFields.includes(registerToFilter.attribute) &&
          registerToFilter.value !== null,
      );
  }

  private async filterCachedRegistry(data: any[]) {
    const cachedString: string = await this.cacheManager.get(this.getStrategyName());

    const parsedObject = cachedString && JSON.parse(cachedString);
    const cachedArray: string[] = parsedObject ? parsedObject.data : [];

    return data.filter((item) => !cachedArray.includes(item.interaction + item.attribute));
  }

  private async setFormsCache(formatedArray: any[]) {
    const data = formatedArray.map((form) => form.interaction + form.attribute);

    const cachedObject = {
      date: DateTime.now().toFormat('yyyy-MM-dd'),
      data,
    };
    await this.cacheManager.set(this.getStrategyName(), JSON.stringify(cachedObject), {
      ttl: 1800,
    });
  }

  private removeBrackets(string: string): string | null {
    if (!string) {
      return null;
    }

    return string.replace(/[\{\}']+/g, '');
  }
}
