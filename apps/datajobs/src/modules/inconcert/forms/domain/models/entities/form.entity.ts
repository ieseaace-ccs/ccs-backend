import { BaseEntity, Entity, PrimaryColumn, Index } from 'typeorm';

@Entity('forms', { schema: 'dbo' })
@Index(['interaction', 'attribute', 'value'], { unique: true })
export class FormsEntity extends BaseEntity {
  @PrimaryColumn('nvarchar', {
    name: 'interaction',
    length: 255
  })
  interaction: string;

  @PrimaryColumn('nvarchar', {
    name: 'attribute',
    length: 255
  })
  attribute: string;

  @PrimaryColumn('varchar', { name: 'value', length: 900 })
  value: string;
}
