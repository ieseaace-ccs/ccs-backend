export class FormOutputDto {
  interaction: string;
  attribute: string;
  value: string;
}
