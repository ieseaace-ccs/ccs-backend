export interface InteractionEntity {
  createFromJson(
    object: Record<string, any>,
    dateFormat: string
  ): InteractionEntity;
}
