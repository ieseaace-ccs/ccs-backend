import { ChatInteractionDto } from '../models/dtos/interaction-chat.dto';
import { PhoneInteractionDto } from '../models/dtos/interaction-phone.dto';

export type Interaction = PhoneInteractionDto | ChatInteractionDto;
