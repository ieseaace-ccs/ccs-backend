import { Controller, Injectable, Logger } from '@nestjs/common';
import { SchedulerRegistry } from '@nestjs/schedule';
import { DateTime } from 'luxon';

import { performance } from 'perf_hooks';
import { InteractionsFactory } from './interactions-factory';

@Injectable()
@Controller()
export class CreateJobService {
  constructor(
    private readonly strategyFactory: InteractionsFactory,
    private readonly schedulerRegistry: SchedulerRegistry,
  ) {}

  async startJob(data): Promise<any> {
    const strategy = this.strategyFactory.createStrategy(data.strategy);
    try {
      const results = [];
      let startDate = DateTime.fromFormat(data.startDate, 'yyyy-MM-dd').plus({ days: -1 });
      const endDate = DateTime.fromFormat(data.endDate, 'yyyy-MM-dd');

      if (!this.schedulerRegistry.getCronJob(strategy.getStrategyName()).running) {
        Logger.debug('Already Running this Batch Process', 'BatchUpsert');
        return 'Already Running a Batch Process';
      }
      this.schedulerRegistry.getCronJob(strategy.getStrategyName()).stop();
      const start = performance.now();
      Logger.debug(
        `Start batch job for ${data.strategy} strategy (${data.startDate} - ${data.endDate})`,
        'BatchUpsert',
      );
      while (startDate < endDate) {
        startDate = startDate.plus({ days: 1 });

        const start = performance.now();
        const fetchedData = await strategy.fetch(startDate.toFormat('yyyy-MM-dd'));
        const formatedData = await strategy.format(fetchedData);
        const insertedData = await strategy.save(formatedData);
        const end = performance.now();

        const duration = parseFloat(((end - start) / 1000).toFixed(2));
        const result = {
          date: startDate.toFormat('dd/MM/yyyy'),
          registers: insertedData.length,
          duration: duration,
        };
        results.push(result);
        Logger.debug(
          `Date: ${startDate.toFormat('yyyy-MM-dd')} - Registers: ${result.registers} - Duration: ${
            result.duration
          }`,
          'BatchUpsert',
        );
      }

      const totalRegisters = results.reduce((prev, curr) => prev + curr.registers, 0);
      const totalSeconds = results.reduce((prev, curr) => prev + curr.duration, 0);

      const totals = {
        date: 'Totals',
        registers: totalRegisters,
        duration: parseFloat(totalSeconds.toFixed(2)),
      };
      results.push(totals);

      const end = performance.now();
      const duration = parseFloat(((end - start) / 1000).toFixed(2));
      Logger.debug(
        `End batch job for ${data.strategy} strategy (${data.startDate} - ${data.endDate}) (${duration}s)`,
      );
      Logger.debug(
        `Upsert Done: Date: ${startDate.toFormat('yyyy-MM-dd')} Registers: ${
          totals.registers
        } Duration: ${totals.duration}`,
        'BatchUpsert',
      );
      this.schedulerRegistry.getCronJob(strategy.getStrategyName()).start();
      return results;
    } catch (error) {
      Logger.error(error);
    } finally {
      this.schedulerRegistry.getCronJob(strategy.getStrategyName()).start();
    }
  }
}
