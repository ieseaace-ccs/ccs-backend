import { Injectable } from '@nestjs/common';
import { PersistorStrategy } from '../../../shared/domain/models/interfaces/inconcert-strategy.interface';
import { AgentsStrategy } from '../../../agents/domain/services/agents-strategy.service';
import { FormsStrategy } from '../../../forms/domain/services/forms-strategy.service';
import { ChatInteractionsStrategy } from './interactions-chat.service';
import { MailInteractionsStrategy } from './interactions-mail.service';
import { PhoneInteractionsStrategy } from './interactions-phone.service';
import { WhatsInteractionsStrategy } from './interactions-whats.service';

@Injectable()
export class InteractionsFactory {
  private readonly strategies = new Map<string, PersistorStrategy>();

  constructor(
    private readonly phoneStrategy: PhoneInteractionsStrategy,
    private readonly chatStrategy: ChatInteractionsStrategy,
    private readonly whatsStrategy: WhatsInteractionsStrategy,
    private readonly mailStrategy: MailInteractionsStrategy,
    private readonly agentsStrategy: AgentsStrategy,
    private readonly formsStrategy: FormsStrategy,
  ) {
    this.strategies.set('phone', this.phoneStrategy);
    this.strategies.set('chat', this.chatStrategy);
    this.strategies.set('whats', this.whatsStrategy);
    this.strategies.set('mail', this.mailStrategy);
    this.strategies.set('agents', this.agentsStrategy);
    this.strategies.set('forms', this.formsStrategy);
  }

  public createStrategy(strategy: string): PersistorStrategy {
    return this.strategies.get(strategy);
  }
}
