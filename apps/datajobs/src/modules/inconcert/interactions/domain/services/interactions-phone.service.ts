import { Injectable, CACHE_MANAGER, Inject } from '@nestjs/common';
import { Cache } from 'cache-manager';
import { InjectRepository } from '@nestjs/typeorm';
import { DateTime } from 'luxon';
import { ConfigService } from '@nestjs/config';

import { InConcertConfigType } from '../../../../config/infrastructure/types/inconcert.type';
import { PersistorStrategy } from '../../../shared/domain/models/interfaces/inconcert-strategy.interface';
import { InconcertApiService } from '../../../shared/infrastructure/services/inconcert.service';
import { PhoneInteractionsRepository } from '../repositories/interactions-phone.repository';
import { PhoneInteractionsEntity } from '../models/entities/interactions-phone.entity';
import { Interaction } from '../contracts/interaction.type';
import { Channels } from '../../../shared/infrastructure/enums/channels.enum';
import { DataPersistenceService } from '../../../shared/domain/services/data-persistence.service';

@Injectable()
export class PhoneInteractionsStrategy implements PersistorStrategy {
  private readonly strategyName = 'Phone Interactions';
  private channel: Channels = Channels.PHONE;
  private readonly config: InConcertConfigType;

  constructor(
    @InjectRepository(PhoneInteractionsRepository)
    private readonly phoneRepository: PhoneInteractionsRepository,
    private readonly inconcertApiService: InconcertApiService,
    private readonly dataPersistenceService: DataPersistenceService,
    @Inject(CACHE_MANAGER) private readonly cacheManager: Cache,
    private readonly configService: ConfigService,
  ) {
    this.config = this.configService.get<InConcertConfigType>('inConcert');
  }

  public getStrategyName() {
    return this.strategyName;
  }

  public async fetch(date: string, channel: Channels = Channels.PHONE): Promise<Interaction[]> {
    const fetchDate = await this.dataPersistenceService.getFetchDate(date, this.getStrategyName());
    this.channel = channel;

    return this.inconcertApiService.fetchInteractions(fetchDate, channel);
  }

  public async format(data: any[]): Promise<Interaction[]> {
    const formatedArray: Interaction[] = data.map((interaction) => {
      const interactionClass = new PhoneInteractionsEntity();
      return interactionClass.createFromJson(interaction, this.config.timeZone);
    });

    const filteredFromCache = await this.filterCachedRegistry(formatedArray);
    await this.setInteractionsCache(formatedArray);
    return filteredFromCache;
  }

  public async save(data: Interaction[]): Promise<Interaction[]> {
    try {
      return await this.phoneRepository.save(data, { chunk: 10 });
    } catch (error) {
      await this.cacheManager.del(this.getStrategyName());
      throw error;
    }
  }

  private async filterCachedRegistry(data: Interaction[]) {
    const cachedString: string = await this.cacheManager.get(this.getStrategyName());

    const parsedObject = cachedString && JSON.parse(cachedString);
    const cachedArray: string[] = parsedObject ? parsedObject.data : [];

    return data.filter((item) => !cachedArray.includes(item.id));
  }

  private async setInteractionsCache(formatedArray: Interaction[]) {
    const data = formatedArray.map((interaction) => interaction.id);

    const cachedObject = {
      date: DateTime.now().toFormat('yyyy-MM-dd'),
      data,
    };
    await this.cacheManager.set(this.getStrategyName(), JSON.stringify(cachedObject), {
      ttl: 1800,
    });
  }
}
