import { Repository, EntityRepository } from 'typeorm';

import { ChatInteractionsEntity } from '../models/entities/interactions-chat.entity';

@EntityRepository(ChatInteractionsEntity)
export class ChatInteractionsRepository extends Repository<ChatInteractionsEntity> {}
