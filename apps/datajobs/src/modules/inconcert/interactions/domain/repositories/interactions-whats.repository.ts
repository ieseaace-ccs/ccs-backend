import { Repository, EntityRepository } from 'typeorm';

import { WhatsInteractionsEntity } from '../models/entities/interactions-whats.entity';

@EntityRepository(WhatsInteractionsEntity)
export class WhatsInteractionsRepository extends Repository<WhatsInteractionsEntity> {}
