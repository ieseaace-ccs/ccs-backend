import { Repository, EntityRepository } from 'typeorm';

import { PhoneInteractionsEntity } from '../models/entities/interactions-phone.entity';

@EntityRepository(PhoneInteractionsEntity)
export class PhoneInteractionsRepository extends Repository<PhoneInteractionsEntity> {}
