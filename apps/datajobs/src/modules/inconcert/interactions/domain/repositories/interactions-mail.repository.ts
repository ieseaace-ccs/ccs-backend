import { Repository, EntityRepository } from 'typeorm';

import { MailInteractionsEntity } from '../models/entities/interactions-mail.entity';

@EntityRepository(MailInteractionsEntity)
export class MailInteractionsRepository extends Repository<MailInteractionsEntity> {}
