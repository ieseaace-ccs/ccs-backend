import { Column, Entity, PrimaryColumn } from 'typeorm';

import {
  booleanFields,
  dateFields,
  whatsInteractionsMap
} from '../../maps/interactions-whatsapp.map';
import { InteractionEntity } from '../../contracts/interaction.interface';
import { CoreEntity } from './core.entity';

@Entity('interactions_whats', { schema: 'dbo' })
export class WhatsInteractionsEntity
  extends CoreEntity
  implements InteractionEntity
{
  @PrimaryColumn('nvarchar', { length: 255 })
  id: string | null;

  @Column('nvarchar', { nullable: true, length: 255 })
  campaign: string | null;

  @Column('datetime', { nullable: true })
  startDate: Date | null;

  @Column('nvarchar', { nullable: true, length: 255 })
  account: string | null;

  @Column('nvarchar', { nullable: true, length: 255 })
  accountId: string | null;

  @Column('nvarchar', { nullable: true, length: 255 })
  lastAttentionLevel: string | null;

  @Column('nvarchar', { nullable: true, length: 255 })
  initiative: string | null;

  @Column('nvarchar', { nullable: true, length: 255 })
  firstAgent: string | null;

  @Column('nvarchar', { nullable: true, length: 255 })
  contactName: string | null;

  @Column('bit', { nullable: true })
  isIvr: boolean | null;

  @Column('bit', { nullable: true })
  pureIvr: boolean | null;

  @Column('bit', { nullable: true })
  hasResponse: boolean | null;

  @Column('datetime', { nullable: true })
  firstResponseDate: Date | null;

  @Column('nvarchar', { nullable: true, length: 255 })
  whatsappTotalMessages: string | null;

  @Column('nvarchar', { nullable: true, length: 255 })
  whatsappTotalClientMessages: string | null;

  @Column('nvarchar', { nullable: true, length: 255 })
  whatsappTotalAgentMessages: string | null;

  @Column('nvarchar', { nullable: true, length: 255 })
  whatsappTotalBotMessages: string | null;

  @Column('nvarchar', { nullable: true, length: 255 })
  whatsappTotalHsmMessages: string | null;

  @Column('datetime', { nullable: true })
  startAttention: Date | null;

  @Column('datetime', { nullable: true })
  endDate: Date | null;

  @Column('float', { nullable: true, precision: 53 })
  durationTime: number | null;

  @Column('float', { nullable: true, precision: 53 })
  ivrTime: number | null;

  @Column('float', { nullable: true, precision: 53 })
  waitingTime: number | null;

  @Column('float', { nullable: true, precision: 53 })
  acdTime: number | null;

  @Column('float', { nullable: true, precision: 53 })
  waitForAnswerTime: number | null;

  @Column('float', { nullable: true, precision: 53 })
  attentionTime: number | null;

  @Column('nvarchar', { nullable: true, length: 255 })
  dispositionCode: string | null;

  @Column('nvarchar', { nullable: true, length: 255 })
  dispositionTreePath: string | null;

  @Column('bit', { nullable: true })
  dispositionIsGoal: boolean | null;

  @Column('nvarchar', { nullable: true, length: 255 })
  contactAddress: string | null;

  @Column('nvarchar', { nullable: true, length: 255 })
  whatsappPostId: string | null;

  @Column('nvarchar', { nullable: true, length: 255 })
  whatsappPostMessage: string | null;

  @Column('nvarchar', { nullable: true, length: 255 })
  whatsappInitialMessageId: string | null;

  @Column('nvarchar', { nullable: true, length: 255 })
  whatsappTypeOfInitialMessage: string | null;

  @Column('varchar', { nullable: true, length: 'max' })
  whatsappTextOfInitialMessage: string | null;

  @Column('nvarchar', { nullable: true, length: 255 })
  isTransferred: string | null;

  @Column('nvarchar', { nullable: true, length: 255 })
  transferSuccessful: string | null;

  @Column('nvarchar', { nullable: true, length: 255 })
  transferDestinationType: string | null;

  @Column('nvarchar', { nullable: true, length: 255 })
  transferDestination: string | null;

  @Column('bit', { nullable: true })
  hasCci: boolean | null;

  @Column('nvarchar', { nullable: true, length: 255 })
  ticket: string | null;

  @Column('nvarchar', { nullable: true, length: 255 })
  vcc: string | null;

  @Column('nvarchar', { nullable: true, length: 255 })
  exportContent: string | null;

  @Column('bit', { nullable: true })
  isDataFromNotifier: boolean | null;

  @Column('datetime', { nullable: true })
  tmStmp: Date | null;

  public createFromJson(jsonObject: Record<string, any>, timeZone: string) {
    const { parseDate, parseBoolean, normalizeHeaders } = this;

    const object = normalizeHeaders(jsonObject, whatsInteractionsMap);
    const restFields = Object.keys(object);

    restFields.forEach((key) => (this[key] = object[key]));
    dateFields.forEach(
      (prop) =>
        (this[prop] = parseDate(object[prop], 'MM/dd/yyyy HH:mm:ss', timeZone))
    );
    booleanFields.forEach((prop) => (this[prop] = parseBoolean(object[prop])));

    return this;
  }
}
