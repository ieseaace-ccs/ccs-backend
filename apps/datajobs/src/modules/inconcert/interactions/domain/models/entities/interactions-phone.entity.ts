import { Column, Entity, PrimaryColumn } from 'typeorm';

import {
  booleanFields,
  withBrackets,
  dateFields,
  phoneInteractionsMap
} from '../../maps/interactions-phone.map';
import { InteractionEntity } from '../../contracts/interaction.interface';
import { CoreEntity } from './core.entity';

@Entity('interactions', { schema: 'dbo' })
export class PhoneInteractionsEntity
  extends CoreEntity
  implements InteractionEntity
{
  @PrimaryColumn('nvarchar', { name: 'Id', nullable: false, length: 255 })
  id: string | null;

  @Column('nvarchar', { name: 'VirtualCC', nullable: true, length: 255 })
  virtualCc: string | null;

  @Column('nvarchar', { name: 'Type', nullable: true, length: 255 })
  type: string | null;

  @Column('datetime', { name: 'StartDate', nullable: true })
  startDate: Date | null;

  @Column('datetime', { name: 'StartDateUTC', nullable: true })
  startDateUtc: Date | null;

  @Column('datetime', { name: 'EndDate', nullable: true })
  endDate: Date | null;

  @Column('datetime', { name: 'EndDateUTC', nullable: true })
  endDateUtc: Date | null;

  @Column('nvarchar', { name: 'Campaign', nullable: true, length: 255 })
  campaign: string | null;

  @Column('nvarchar', { name: 'Direction', nullable: true, length: 255 })
  direction: string | null;

  @Column('nvarchar', { name: 'Sections', nullable: true, length: 255 })
  sections: string | null;

  @Column('bit', { name: 'IsTaked', nullable: true })
  isTaked: boolean | null;

  @Column('bit', { name: 'IsAbandoned', nullable: true })
  isAbandoned: boolean | null;

  @Column('bit', { name: 'IsCancelled', nullable: true })
  isCancelled: boolean | null;

  @Column('bit', { name: 'IsInBoundAttended', nullable: true })
  isInBoundAttended: boolean | null;

  @Column('bit', { name: 'IsOutBoundAttended', nullable: true })
  isOutBoundAttended: boolean | null;

  @Column('bit', { name: 'IsInBoundAbandoned', nullable: true })
  isInBoundAbandoned: boolean | null;

  @Column('bit', { name: 'IsOutBoundAbandoned', nullable: true })
  isOutBoundAbandoned: boolean | null;

  @Column('bit', { name: 'IsInBoundCancelled', nullable: true })
  isInBoundCancelled: boolean | null;

  @Column('bit', { name: 'IsOutBoundCancelled', nullable: true })
  isOutBoundCancelled: boolean | null;

  @Column('bit', { name: 'IsTransferred', nullable: true })
  isTransferred: boolean | null;

  @Column('nvarchar', { name: 'TransferType', nullable: true, length: 255 })
  transferType: string | null;

  @Column('nvarchar', { name: 'TransferResult', nullable: true, length: 255 })
  transferResult: string | null;

  @Column('nvarchar', {
    name: 'NumberOfTransferred',
    nullable: true,
    length: 255
  })
  numberOfTransferred: string | null;

  @Column('bit', { name: 'IsConferenced', nullable: true })
  isConferenced: boolean | null;

  @Column('nvarchar', {
    name: 'NumbersOfConferenced',
    nullable: true,
    length: 255
  })
  numbersOfConferenced: string | null;

  @Column('bit', { name: 'IsCallBack', nullable: true })
  isCallBack: boolean | null;

  @Column('bit', { name: 'HasCallBack', nullable: true })
  hasCallBack: boolean | null;

  @Column('bit', { name: 'HasVoiceMail', nullable: true })
  hasVoiceMail: boolean | null;

  @Column('bit', { name: 'SLPossitive', nullable: true })
  slPossitive: boolean | null;

  @Column('datetime', { name: 'TimeStamp', nullable: true })
  timeStamp: Date | null;

  @Column('datetime', { name: 'TimeStampUTC', nullable: true })
  timeStampUtc: Date | null;

  @Column('float', { name: 'DurationTime', nullable: true, precision: 53 })
  durationTime: number | null;

  @Column('float', { name: 'AnswerTime', nullable: true, precision: 53 })
  answerTime: number | null;

  @Column('float', { name: 'WaitTime', nullable: true, precision: 53 })
  waitTime: number | null;

  @Column('float', { name: 'ControlAgentTime', nullable: true, precision: 53 })
  controlAgentTime: number | null;

  @Column('float', { name: 'IVRTime', nullable: true, precision: 53 })
  ivrTime: number | null;

  @Column('float', { name: 'AttentionTime', nullable: true, precision: 53 })
  attentionTime: number | null;

  @Column('float', { name: 'WrapupTime', nullable: true, precision: 53 })
  wrapupTime: number | null;

  @Column('float', { name: 'DesertionTime', nullable: true, precision: 53 })
  desertionTime: number | null;

  @Column('float', { name: 'HoldTime', nullable: true, precision: 53 })
  holdTime: number | null;

  @Column('float', { name: 'TransferTime', nullable: true, precision: 53 })
  transferTime: number | null;

  @Column('float', { name: 'RequeuedTime', nullable: true, precision: 53 })
  requeuedTime: number | null;

  @Column('float', { name: 'RingingTime', nullable: true, precision: 53 })
  ringingTime: number | null;

  @Column('float', { name: 'RingBackTime', nullable: true, precision: 53 })
  ringBackTime: number | null;

  @Column('bit', { name: 'OutOfHour', nullable: true })
  outOfHour: boolean | null;

  @Column('nvarchar', { name: 'Shift', nullable: true, length: 255 })
  shift: string | null;

  @Column('nvarchar', { name: 'Process', nullable: true, length: 255 })
  process: string | null;

  @Column('float', { name: 'TotalHolds', nullable: true, precision: 53 })
  totalHolds: number | null;

  @Column('bit', { name: 'IsShortCallThreshold', nullable: true })
  isShortCallThreshold: boolean | null;

  @Column('bit', { name: 'IsLongCallThreshold', nullable: true })
  isLongCallThreshold: boolean | null;

  @Column('bit', { name: 'IsGhostCallThresHold', nullable: true })
  isGhostCallThresHold: boolean | null;

  @Column('bit', { name: 'IsIVR', nullable: true })
  isIvr: boolean | null;

  @Column('bit', { name: 'IsSentToAgentSearch', nullable: true })
  isSentToAgentSearch: boolean | null;

  @Column('nvarchar', { name: 'TrunkId', nullable: true, length: 255 })
  trunkId: string | null;

  @Column('nvarchar', { name: 'TrunkType', nullable: true, length: 255 })
  trunkType: string | null;

  @Column('nvarchar', { name: 'Prefix', nullable: true, length: 255 })
  prefix: string | null;

  @Column('nvarchar', { name: 'OriginalCampaign', nullable: true, length: 255 })
  originalCampaign: string | null;

  @Column('nvarchar', { name: 'ForwardProcess', nullable: true, length: 255 })
  forwardProcess: string | null;

  @Column('nvarchar', { name: 'RedialProcess', nullable: true, length: 255 })
  redialProcess: string | null;

  @Column('datetime', { name: 'StartAttentionTime', nullable: true })
  startAttentionTime: Date | null;

  @Column('datetime', { name: 'StartAttentionTimeUTC', nullable: true })
  startAttentionTimeUtc: Date | null;

  @Column('nvarchar', { name: 'Disposition', nullable: true, length: 255 })
  disposition: string | null;

  @Column('nvarchar', { name: 'ContactId', nullable: true, length: 255 })
  contactId: string | null;

  @Column('nvarchar', { name: 'ContactName', nullable: true, length: 255 })
  contactName: string | null;

  @Column('nvarchar', { name: 'ContactAddress', nullable: true, length: 255 })
  contactAddress: string | null;

  @Column('nvarchar', { name: 'ManagementResult', nullable: true, length: 255 })
  managementResult: string | null;
  @Column('nvarchar', {
    name: 'DispositionTreePath',
    nullable: true,
    length: 255
  })
  dispositionTreePath: string | null;

  @Column('bit', { name: 'IsGoalManagementResult', nullable: true })
  isGoalManagementResult: boolean | null;

  @Column('bit', { name: 'Completed', nullable: true })
  completed: boolean | null;

  @Column('nvarchar', { name: 'FirstAgent', nullable: true, length: 255 })
  firstAgent: string | null;

  @Column('nvarchar', { name: 'LastAgent', nullable: true, length: 255 })
  lastAgent: string | null;

  @Column('float', { name: 'TotalAgents', nullable: true, precision: 53 })
  totalAgents: number | null;

  @Column('float', { name: 'Slice', nullable: true, precision: 53 })
  slice: number | null;

  @Column('nvarchar', { name: 'WorkitemType', nullable: true, length: 255 })
  workitemType: string | null;

  @Column('nvarchar', { name: 'BatchId', nullable: true, length: 255 })
  batchId: string | null;

  @Column('nvarchar', {
    name: 'OutboundProcessId',
    nullable: true,
    length: 255
  })
  outboundProcessId: string | null;

  @Column('bit', { name: 'IsUseful', nullable: true })
  isUseful: boolean | null;

  @Column('float', { name: 'PreviewTime', nullable: true, precision: 53 })
  previewTime: number | null;

  @Column('float', { name: 'Section', nullable: true, precision: 53 })
  section: number | null;

  @Column('nvarchar', { name: 'Actor', nullable: true, length: 255 })
  actor: string | null;

  @Column('nvarchar', { name: 'ActorName', nullable: true, length: 255 })
  actorName: string | null;

  @Column('float', { name: 'Part', nullable: true, precision: 53 })
  part: number | null;

  @Column('nvarchar', {
    name: 'PreferredCampaign',
    nullable: true,
    length: 255
  })
  preferredCampaign: string | null;

  @Column('float', { name: 'FirstAtteActor', nullable: true, precision: 53 })
  firstAtteActor: number | null;

  @Column('nvarchar', { name: 'SourceHangup', nullable: true, length: 255 })
  sourceHangup: string | null;

  public createFromJson(jsonObject: Record<string, any>, timeZone: string) {
    const { removeBrackets, parseDate, parseBoolean, normalizeHeaders } = this;

    const object = normalizeHeaders(jsonObject, phoneInteractionsMap);
    const restFields = Object.keys(object);

    restFields.forEach((key) => (this[key] = object[key]));
    withBrackets.forEach((prop) => (this[prop] = removeBrackets(object[prop])));
    dateFields.forEach(
      (prop) =>
        (this[prop] = parseDate(object[prop], 'dd/MM/yyyy HH:mm:ss', timeZone))
    );
    booleanFields.forEach((prop) => (this[prop] = parseBoolean(object[prop])));

    return this;
  }
}
