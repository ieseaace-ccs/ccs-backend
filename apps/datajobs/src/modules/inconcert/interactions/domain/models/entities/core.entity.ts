import { BaseEntity } from 'typeorm';
import { DateTime } from 'luxon';

export class CoreEntity extends BaseEntity {
  protected normalizeHeaders(record: any, map: any): any {
    return Object.keys(record)
      .map((key) => {
        const newKey = map[key] || key;
        return {
          [newKey]: record[key]
        };
      })
      .reduce((a, b) => Object.assign({}, a, b));
  }

  protected parseDate(
    date: string,
    format = 'dd/MM/yyyy HH:mm:ss',
    timeZone = 'America/Mexico_City'
  ): Date {
    if (!date) {
      return null;
    }

    return DateTime.fromFormat(date, format, {
      zone: timeZone
    }).toJSDate();
  }

  protected parseBoolean(value: any): boolean {
    if (['True', 'true', '1', 1, true].includes(value)) {
      return true;
    }
    return false;
  }

  protected removeBrackets(string: string): string | null {
    if (!string) {
      return null;
    }
    return string.replace(/[\{\}']+/g, '');
  }
}
