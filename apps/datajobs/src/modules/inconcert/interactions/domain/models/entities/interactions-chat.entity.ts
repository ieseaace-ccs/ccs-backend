import { Column, Entity, PrimaryColumn } from 'typeorm';

import {
  booleanFields,
  dateFields,
  chatInteractionsMap
} from '../../maps/interactions-chat.map';
import { InteractionEntity } from '../../contracts/interaction.interface';
import { CoreEntity } from './core.entity';

@Entity('interactions_chat', { schema: 'dbo' })
export class ChatInteractionsEntity
  extends CoreEntity
  implements InteractionEntity
{
  @PrimaryColumn('nvarchar', { length: 255 })
  id: string | null;

  @Column('nvarchar', { nullable: true, length: 255 })
  vcc: string | null;

  @Column('nvarchar', { nullable: true, length: 255 })
  campaign: string | null;

  @Column('datetime', { nullable: true })
  startDate: Date | null;

  @Column('nvarchar', { nullable: true, length: 255 })
  account: string | null;

  @Column('nvarchar', { nullable: true, length: 255 })
  accountId: string | null;

  @Column('nvarchar', { nullable: true, length: 255 })
  lastAttentionLevel: string | null;

  @Column('nvarchar', { nullable: true, length: 255 })
  contactName: string | null;

  @Column('nvarchar', { nullable: true, length: 255 })
  firstTakeAgent: string | null;

  @Column('nvarchar', { nullable: true, length: 255 })
  lastTakeAgent: string | null;

  @Column('bit', { nullable: true })
  isIvr: boolean | null;

  @Column('bit', { nullable: true })
  pureIvr: boolean | null;

  @Column('bit', { nullable: true })
  isTaken: boolean | null;

  @Column('bit', { nullable: true })
  isFinished: boolean | null;

  @Column('bit', { nullable: true })
  isAbandoned: boolean | null;

  @Column('bit', { nullable: true })
  slPositive: boolean | null;

  @Column('bit', { nullable: true })
  hasAnswerFailures: boolean | null;

  @Column('bit', { nullable: true })
  isGhost: boolean | null;

  @Column('bit', { nullable: true })
  isOutofScheduler: boolean | null;

  @Column('nvarchar', { nullable: true, length: 255 })
  ender: string | null;

  @Column('datetime', { nullable: true })
  startAttention: Date | null;

  @Column('datetime', { nullable: true })
  endDate: Date | null;

  @Column('float', { nullable: true, precision: 53 })
  durationTime: number | null;

  @Column('float', { nullable: true, precision: 53 })
  ivrTime: number | null;

  @Column('float', { nullable: true, precision: 53 })
  waitingTime: number | null;

  @Column('float', { nullable: true, precision: 53 })
  acdTime: number | null;

  @Column('float', { nullable: true, precision: 53 })
  waitForAnswerTime: number | null;

  @Column('float', { nullable: true, precision: 53 })
  attentionTime: number | null;

  @Column('float', { nullable: true, precision: 53 })
  wrapupTime: number | null;

  @Column('float', { nullable: true, precision: 53 })
  desertionTime: number | null;

  @Column('nvarchar', { nullable: true, length: 255 })
  dispositionCode: string | null;
  @Column('nvarchar', { nullable: true, length: 255 })
  dispositionTreePath: string | null;

  @Column('bit', { nullable: true })
  dispositionIsGoal: boolean | null;

  @Column('bit', { nullable: true })
  isTransferred: boolean | null;

  @Column('nvarchar', { nullable: true, length: 255 })
  transferSuccessful: string | null;

  @Column('nvarchar', { nullable: true, length: 255 })
  transferDestinationType: string | null;

  @Column('nvarchar', { nullable: true, length: 255 })
  transferDestination: string | null;

  @Column('nvarchar', { nullable: true, length: 255 })
  chatMessages: string | null;

  @Column('nvarchar', { nullable: true, length: 255 })
  chatClientMessages: string | null;

  @Column('nvarchar', { nullable: true, length: 255 })
  chatAgentMessages: string | null;

  @Column('nvarchar', { nullable: true, length: 255 })
  chatBotMessages: string | null;

  @Column('bit', { nullable: true })
  hasLogin: boolean | null;

  @Column('bit', { nullable: true })
  hasPreSurvey: boolean | null;

  @Column('bit', { nullable: true })
  hasPostSurvey: boolean | null;

  @Column('bit', { nullable: true })
  hasCci: boolean | null;

  @Column('nvarchar', { nullable: true, length: 255 })
  ticket: string | null;

  @Column('nvarchar', { nullable: true, length: 255 })
  exportContent: string | null;

  @Column('datetime', { nullable: true })
  startWrapupTime: Date | null;

  @Column('bit', { nullable: true })
  isDataFromNotifier: boolean | null;

  @Column('datetime', { nullable: true })
  startTakenTime: Date | null;

  @Column('datetime', { nullable: true })
  tmStmp: Date | null;

  public createFromJson(jsonObject: Record<string, any>, timeZone: string) {
    const { parseDate, parseBoolean, normalizeHeaders } = this;

    const object = normalizeHeaders(jsonObject, chatInteractionsMap);
    const restFields = Object.keys(object);

    restFields.forEach((key) => (this[key] = object[key]));

    dateFields.forEach(
      (prop) =>
        (this[prop] = parseDate(object[prop], 'MM/dd/yyyy HH:mm:ss', timeZone))
    );
    booleanFields.forEach((prop) => (this[prop] = parseBoolean(object[prop])));

    return this;
  }
}
