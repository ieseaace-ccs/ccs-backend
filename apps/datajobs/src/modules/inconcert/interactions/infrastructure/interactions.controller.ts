import { Controller, Injectable } from '@nestjs/common';
import { MessagePattern } from '@nestjs/microservices';

import { CreateJobService } from '../domain/services/bulk-update';

@Injectable()
@Controller()
export class JobsController {
  constructor(private readonly jobService: CreateJobService) {}
  @MessagePattern({ cmd: 'jobs' })
  public handle(data) {
    return this.jobService.startJob(data);
  }
}
