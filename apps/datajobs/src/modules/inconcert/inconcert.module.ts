import { TerminusModule } from '@nestjs/terminus';
import { HealthController } from './shared/infrastructure/controllers/health.controller';
import { HttpModule, Module, CacheModule } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { CronjobsController } from './shared/infrastructure/controllers/cronjobs.controller';
import { InconcertApiService } from './shared/infrastructure/services/inconcert.service';
import { DataPersistenceService } from './shared/domain/services/data-persistence.service';
import { PhoneInteractionsStrategy } from './interactions/domain/services/interactions-phone.service';
import { ChatInteractionsStrategy } from './interactions/domain/services/interactions-chat.service';
import { WhatsInteractionsStrategy } from './interactions/domain/services/interactions-whats.service';
import { MailInteractionsStrategy } from './interactions/domain/services/interactions-mail.service';
import { AgentsStrategy } from './agents/domain/services/agents-strategy.service';
import { FormsStrategy } from './forms/domain/services/forms-strategy.service';
import { PhoneInteractionsRepository } from './interactions/domain/repositories/interactions-phone.repository';
import { ChatInteractionsRepository } from './interactions/domain/repositories/interactions-chat.repository';
import { WhatsInteractionsRepository } from './interactions/domain/repositories/interactions-whats.repository';
import { MailInteractionsRepository } from './interactions/domain/repositories/interactions-mail.repository';
import { AgentsRepository } from './agents/domain/repositories/agents.repository';
import { FormsRepository } from './forms/domain/repositories/forms.repository';
import { JobsController } from './interactions/infrastructure/interactions.controller';
import { CreateJobService } from './interactions/domain/services/bulk-update';
import { InteractionsFactory } from './interactions/domain/services/interactions-factory';
@Module({
  imports: [
    TerminusModule,
    TypeOrmModule.forFeature([
      PhoneInteractionsRepository,
      ChatInteractionsRepository,
      WhatsInteractionsRepository,
      MailInteractionsRepository,
      AgentsRepository,
      FormsRepository,
    ]),
    CacheModule.register(),
    HttpModule,
  ],
  controllers: [HealthController, JobsController],
  providers: [
    CronjobsController,
    DataPersistenceService,
    PhoneInteractionsStrategy,
    ChatInteractionsStrategy,
    WhatsInteractionsStrategy,
    MailInteractionsStrategy,
    AgentsStrategy,
    FormsStrategy,
    InconcertApiService,
    CreateJobService,
    InteractionsFactory,
  ],
})
export class InconcertModule {}
