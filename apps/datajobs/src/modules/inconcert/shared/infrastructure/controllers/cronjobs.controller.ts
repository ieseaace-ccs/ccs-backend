import { Injectable, Logger } from '@nestjs/common';
import { SchedulerRegistry } from '@nestjs/schedule';
import { ConfigService } from '@nestjs/config';
import { CronJob } from 'cron';
import { InjectSentry, SentryService } from '@ntegral/nestjs-sentry';

import { DataPersistenceService } from '../../domain/services/data-persistence.service';
import { CronjobsConfigType } from '../../../../config/infrastructure/types/cronjobs.type';
import { PersistorStrategy } from '../../domain/models/interfaces/inconcert-strategy.interface';
import { PhoneInteractionsStrategy } from '../../../interactions/domain/services/interactions-phone.service';
import { ChatInteractionsStrategy } from '../../../interactions/domain/services/interactions-chat.service';
import { WhatsInteractionsStrategy } from '../../../interactions/domain/services/interactions-whats.service';
import { MailInteractionsStrategy } from '../../../interactions/domain/services/interactions-mail.service';
import { AgentsStrategy } from './../../../agents/domain/services/agents-strategy.service';
import { FormsStrategy } from './../../../forms/domain/services/forms-strategy.service';

@Injectable()
export class CronjobsController {
  private readonly config: CronjobsConfigType;

  constructor(
    private readonly configService: ConfigService,
    private readonly dataPersistenceService: DataPersistenceService,
    private readonly schedulerRegistry: SchedulerRegistry,
    private readonly phoneStrategy: PhoneInteractionsStrategy,
    private readonly chatStrategy: ChatInteractionsStrategy,
    private readonly whatsStrategy: WhatsInteractionsStrategy,
    private readonly mailStrategy: MailInteractionsStrategy,
    private readonly agentsStrategy: AgentsStrategy,
    private readonly formsStrategy: FormsStrategy,
    @InjectSentry() private readonly client: SentryService,
  ) {
    this.config = this.configService.get<CronjobsConfigType>('cronjobs');
  }
  private readonly logger = new Logger(CronjobsController.name);

  protected onModuleInit() {
    const {
      phoneInteractionsCron,
      chatInteractionsCron,
      whatsInteractionsCron,
      mailInteractionsCron,
      agentsCron,
      formsCron,
    } = this.config;

    this.addCronJob(
      this.phoneStrategy.getStrategyName(),
      this.generateCron(this.phoneStrategy, phoneInteractionsCron),
    );
    this.addCronJob(
      this.chatStrategy.getStrategyName(),
      this.generateCron(this.chatStrategy, chatInteractionsCron),
    );
    this.addCronJob(
      this.whatsStrategy.getStrategyName(),
      this.generateCron(this.whatsStrategy, whatsInteractionsCron),
    );
    this.addCronJob(
      this.mailStrategy.getStrategyName(),
      this.generateCron(this.mailStrategy, mailInteractionsCron),
    );
    this.addCronJob(
      this.agentsStrategy.getStrategyName(),
      this.generateCron(this.agentsStrategy, agentsCron),
    );
    this.addCronJob(
      this.formsStrategy.getStrategyName(),
      this.generateCron(this.formsStrategy, formsCron),
    );
  }

  private addCronJob(name: string, job: any) {
    this.schedulerRegistry.addCronJob(name, job);
    job.start();
  }

  private generateCron(strategy: PersistorStrategy, crontab: string): CronJob {
    return new CronJob(crontab, async () => {
      try {
        const results = await this.dataPersistenceService.dataPersistor(strategy);
        this.logger.debug(
          `${strategy.getStrategyName()} Strategy (${crontab}), fetched ${
            results.fetched
          } registries, inserted/updated ${results.inserted} registries`,
        );
      } catch (error) {
        if (error.code === 'ESOCKET') {
          this.client.instance().setTag('CronJobs', 'SQL');
          this.client.instance().captureException(error);
          this.logger.error(error.message);
        } else {
          throw error;
        }
      }
    });
  }
}
