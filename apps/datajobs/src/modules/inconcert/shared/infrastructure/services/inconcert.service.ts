import { ConfigService } from '@nestjs/config';
import { HttpService, Injectable } from '@nestjs/common';
import { InjectSentry, SentryService } from '@ntegral/nestjs-sentry';

import { InConcertConfigType } from '../../../../config/infrastructure/types/inconcert.type';
import { Channel } from 'aws-sdk/clients/connect';
import { Channels } from '../enums/channels.enum';

@Injectable()
export class InconcertApiService {
  private readonly config: InConcertConfigType;
  constructor(
    private readonly configService: ConfigService,
    private readonly httpService: HttpService,
    @InjectSentry() private readonly client: SentryService,
  ) {
    this.config = this.configService.get<InConcertConfigType>('inConcert');
  }

  public async fetchInteractions(date: string, channel: Channel = Channels.PHONE): Promise<any> {
    const { baseUrl, interactionsUri, apiKey, pageLimit } = this.config;
    const options = {
      headers: {
        'x-api-key': apiKey,
      },
    };

    let page = 1;
    let hasMorePages = true;
    const resultItems = [];

    while (hasMorePages) {
      const uri = `${baseUrl}/ccs/${interactionsUri}/${channel}/${date}/${page}`;
      const response = await this.httpService.get<any>(uri, options).toPromise();

      const responseLength = response.data.items.length;
      resultItems.push(...response.data.items);
      hasMorePages = false;

      if (responseLength >= pageLimit) {
        hasMorePages = true;
        page += 1;
      }
    }

    return Promise.resolve(resultItems);
  }

  public async fetchAgents(date: string, channel: Channels): Promise<any> {
    const { baseUrl, agentsUri, apiKey } = this.config;
    const options = {
      headers: {
        'x-api-key': apiKey,
      },
    };

    const uri = `${baseUrl}/ccs/${agentsUri}/${date}`;
    const response = await this.httpService.get<any>(uri, options).toPromise();
    response.data.channel = channel;
    const responseItems = response.data.items;
    return Promise.resolve(responseItems);
  }

  public async fetchForms(
    date: string,
    campaign: string,
    channel: Channels = Channels.PHONE,
  ): Promise<any> {
    try {
      const { baseUrl, interactionsUri, apiKey, pageLimit } = this.config;
      const options = {
        headers: {
          'x-api-key': apiKey,
        },
      };

      let page = 1;
      let hasMorePages = true;
      const resultItems = [];

      while (hasMorePages) {
        const uri = `${baseUrl}/${campaign}${interactionsUri}/${channel}/${date}/${page}`;
        const response = await this.httpService.get<any>(uri, options).toPromise();

        const responseLength = response.data.items.length;
        resultItems.push(...response.data.items);
        hasMorePages = false;

        if (responseLength >= pageLimit) {
          hasMorePages = true;
          page += 1;
        }
      }

      return Promise.resolve(resultItems);
    } catch (error) {
      const { response } = error;
      if (response && response.status === 403) {
        this.client.instance().setTag('Type', 'DeprecatedCampaign');
        this.client.instance().setTag('Campaign', campaign);
        this.client.instance().captureMessage(error);
        return Promise.resolve([]);
      }
      throw error;
    }
  }
}
