import { Injectable, CACHE_MANAGER, Inject } from '@nestjs/common';
import { Cache } from 'cache-manager';
import { SchedulerRegistry } from '@nestjs/schedule';
import { DateTime } from 'luxon';

import { PersistorStrategy } from '../models/interfaces/inconcert-strategy.interface';
import { Interaction } from '../../../interactions/domain/contracts/interaction.type';
import { Channels } from '../../infrastructure/enums/channels.enum';
import { ConfigService } from '@nestjs/config';

@Injectable()
export class DataPersistenceService {
  constructor(
    private readonly schedulerRegistry: SchedulerRegistry,
    @Inject(CACHE_MANAGER) private readonly cacheManager: Cache,
    private readonly config: ConfigService,
  ) {}
  public async dataPersistor(
    strategy: PersistorStrategy,
    date: Date = new Date(),
    channel?: Channels,
  ): Promise<any> {
    try {
      this.schedulerRegistry.getCronJob(strategy.getStrategyName()).stop();
      const parsedDate = DateTime.fromJSDate(date)
        .setZone(this.config.get('inConcert').localTimeZone)
        .toFormat('yyyy-MM-dd');

      const fetchedData: Interaction[] = await strategy.fetch(parsedDate, channel);
      const formatedData: Interaction[] = await strategy.format(fetchedData);
      const insertedData: Interaction[] = await strategy.save(formatedData);
      this.schedulerRegistry.getCronJob(strategy.getStrategyName()).start();

      return { fetched: fetchedData.length, inserted: insertedData.length };
    } catch (error) {
      this.schedulerRegistry.getCronJob(strategy.getStrategyName()).start();
      throw error;
    }
  }

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  public async getFetchDate(date: string, strategy: string) {
    // let fetchDate: string = date;

    // const cachedString: string = await this.cacheManager.get(strategy);

    // const parsedObject = cachedString && JSON.parse(cachedString);

    // const yesterday = DateTime.now()
    //   .setZone(this.config.get('inConcert').localTimeZone)
    //   .minus({ days: 1 })
    //   .toFormat('yyyy-MM-dd');

    // const cachedDate: string = parsedObject ? parsedObject.date : yesterday;

    // if (cachedDate === yesterday) {
    //   fetchDate = yesterday;
    // }
    return date;
  }
}
