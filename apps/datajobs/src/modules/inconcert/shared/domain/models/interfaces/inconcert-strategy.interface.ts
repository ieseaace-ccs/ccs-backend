import { Channels } from '../../../infrastructure/enums/channels.enum';

export interface PersistorStrategy {
  getStrategyName(): string;
  fetch(data: string, channel?: Channels): Promise<any>;
  format(data: any[]): Promise<any[]>;
  save(data: any[]): Promise<any[]>;
}
