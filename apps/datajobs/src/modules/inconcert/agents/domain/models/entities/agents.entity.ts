import { Column, Entity, PrimaryColumn } from 'typeorm';

@Entity('agents', { schema: 'dbo' })
export class AgentsEntity {
  @PrimaryColumn('nvarchar', { name: 'Actor', length: 255 })
  actor: string | null;

  @Column('nvarchar', { name: 'VirtualCC', nullable: true, length: 255 })
  virtualCc: string | null;

  @PrimaryColumn('datetime', { name: 'Date' })
  date: Date | null;

  @Column('int', { name: 'TotalBreaks', nullable: true })
  totalBreaks: number | null;

  @Column('int', { name: 'TotalToilettes', nullable: true })
  totalToilettes: number | null;

  @Column('int', { name: 'TotalTrainings', nullable: true })
  totalTrainings: number | null;

  @Column('int', { name: 'TotalPersonals', nullable: true })
  totalPersonals: number | null;

  @Column('int', { name: 'TotalCustoms', nullable: true })
  totalCustoms: number | null;

  @Column('int', { name: 'TotalPauses', nullable: true })
  totalPauses: number | null;

  @Column('int', { name: 'TotalLogins', nullable: true })
  totalLogins: number | null;

  @Column('decimal', {
    name: 'LoggedState',
    nullable: true,
    precision: 18,
    scale: 0
  })
  loggedState: number | null;

  @Column('decimal', {
    name: 'ActiveState',
    nullable: true,
    precision: 18,
    scale: 0
  })
  activeState: number | null;

  @Column('decimal', {
    name: 'BreakState',
    nullable: true,
    precision: 18,
    scale: 0
  })
  breakState: number | null;

  @Column('decimal', {
    name: 'PauseState',
    nullable: true,
    precision: 18,
    scale: 0
  })
  pauseState: number | null;

  @Column('decimal', {
    name: 'AttePauseState',
    nullable: true,
    precision: 18,
    scale: 0
  })
  attePauseState: number | null;

  @Column('decimal', {
    name: 'WrapupPauseState',
    nullable: true,
    precision: 18,
    scale: 0
  })
  wrapupPauseState: number | null;

  @Column('decimal', {
    name: 'TrainingState',
    nullable: true,
    precision: 18,
    scale: 0
  })
  trainingState: number | null;

  @Column('decimal', {
    name: 'PersonalState',
    nullable: true,
    precision: 18,
    scale: 0
  })
  personalState: number | null;

  @Column('decimal', {
    name: 'ToiletteState',
    nullable: true,
    precision: 18,
    scale: 0
  })
  toiletteState: number | null;

  @Column('decimal', {
    name: 'CustomsState',
    nullable: true,
    precision: 18,
    scale: 0
  })
  customsState: number | null;

  @PrimaryColumn('nvarchar', {
    name: 'PreferredCampaign',
    length: 255
  })
  preferredCampaign: string | null;

  @Column('decimal', {
    name: 'TotalUnavailable',
    nullable: true,
    precision: 18,
    scale: 0
  })
  totalUnavailable: number | null;

  @Column('decimal', {
    name: 'UnavailableState',
    nullable: true,
    precision: 18,
    scale: 0
  })
  unavailableState: number | null;

  @Column('decimal', {
    name: 'AtteActiveState',
    nullable: true,
    precision: 18,
    scale: 0
  })
  atteActiveState: number | null;

  @Column('decimal', {
    name: 'WrapupActiveState',
    nullable: true,
    precision: 18,
    scale: 0
  })
  wrapupActiveState: number | null;

  @Column('int', { name: 'DurationCount', nullable: true })
  durationCount: number | null;

  @Column('decimal', {
    name: 'DurationTime',
    nullable: true,
    precision: 18,
    scale: 0
  })
  durationTime: number | null;

  @Column('int', { name: 'AnswerCount', nullable: true })
  answerCount: number | null;

  @Column('decimal', {
    name: 'AnswerTime',
    nullable: true,
    precision: 18,
    scale: 0
  })
  answerTime: number | null;

  @Column('int', { name: 'WaitCount', nullable: true })
  waitCount: number | null;

  @Column('decimal', {
    name: 'WaitTime',
    nullable: true,
    precision: 18,
    scale: 0
  })
  waitTime: number | null;

  @Column('int', { name: 'ControlAgentCount', nullable: true })
  controlAgentCount: number | null;

  @Column('decimal', {
    name: 'ControlAgentTime',
    nullable: true,
    precision: 18,
    scale: 0
  })
  controlAgentTime: number | null;

  @Column('int', { name: 'AttentionCount', nullable: true })
  attentionCount: number | null;

  @Column('decimal', {
    name: 'AttentionTime',
    nullable: true,
    precision: 18,
    scale: 0
  })
  attentionTime: number | null;

  @Column('int', { name: 'WrapupCount', nullable: true })
  wrapupCount: number | null;

  @Column('decimal', {
    name: 'WrapupTime',
    nullable: true,
    precision: 18,
    scale: 0
  })
  wrapupTime: number | null;

  @Column('int', { name: 'DesertionCount', nullable: true })
  desertionCount: number | null;

  @Column('decimal', {
    name: 'DesertionTime',
    nullable: true,
    precision: 18,
    scale: 0
  })
  desertionTime: number | null;

  @Column('int', { name: 'HoldCount', nullable: true })
  holdCount: number | null;

  @Column('decimal', {
    name: 'HoldTime',
    nullable: true,
    precision: 18,
    scale: 0
  })
  holdTime: number | null;

  @Column('int', { name: 'TransferCount', nullable: true })
  transferCount: number | null;

  @Column('decimal', {
    name: 'TransferTime',
    nullable: true,
    precision: 18,
    scale: 0
  })
  transferTime: number | null;

  @Column('int', { name: 'RequeuedCount', nullable: true })
  requeuedCount: number | null;

  @Column('decimal', {
    name: 'RequeuedTime',
    nullable: true,
    precision: 18,
    scale: 0
  })
  requeuedTime: number | null;

  @Column('int', { name: 'RingingCount', nullable: true })
  ringingCount: number | null;

  @Column('decimal', {
    name: 'RingingTime',
    nullable: true,
    precision: 18,
    scale: 0
  })
  ringingTime: number | null;

  @Column('int', { name: 'RingBackCount', nullable: true })
  ringBackCount: number | null;

  @Column('decimal', {
    name: 'RingBackTime',
    nullable: true,
    precision: 18,
    scale: 0
  })
  ringBackTime: number | null;

  @Column('int', { name: 'PreviewCount', nullable: true })
  previewCount: number | null;

  @Column('decimal', {
    name: 'PreviewTime',
    nullable: true,
    precision: 18,
    scale: 0
  })
  previewTime: number | null;
}
