import { ConfigService } from '@nestjs/config';
import { Injectable, CACHE_MANAGER, Inject } from '@nestjs/common';
import { Cache } from 'cache-manager';
import { InjectRepository } from '@nestjs/typeorm';
import { DateTime } from 'luxon';

import { InConcertConfigType } from '../../../../config/infrastructure/types/inconcert.type';
import { PersistorStrategy } from '../../../shared/domain/models/interfaces/inconcert-strategy.interface';
import { AgentDto } from '../models/dtos/agent.dto';
import { InconcertApiService } from '../../../shared/infrastructure/services/inconcert.service';
import { AgentsRepository } from '../repositories/agents.repository';
import { DataPersistenceService } from '../../../shared/domain/services/data-persistence.service';
import { Channels } from '../../../shared/infrastructure/enums/channels.enum';

@Injectable()
export class AgentsStrategy implements PersistorStrategy {
  private readonly strategyName = 'Agents';
  private readonly config: InConcertConfigType;
  constructor(
    @InjectRepository(AgentsRepository)
    private readonly agentsRepository: AgentsRepository,
    private readonly inconcertApiService: InconcertApiService,
    private readonly configService: ConfigService,
    private readonly dataPersistenceService: DataPersistenceService,
    @Inject(CACHE_MANAGER) private readonly cacheManager: Cache,
  ) {
    this.config = this.configService.get<InConcertConfigType>('inConcert');
  }

  public getStrategyName() {
    return this.strategyName;
  }

  public async fetch(date: string, channel: Channels = Channels.PHONE): Promise<AgentDto[]> {
    const fetchDate = await this.dataPersistenceService.getFetchDate(date, this.getStrategyName());
    return this.inconcertApiService.fetchAgents(fetchDate, channel);
  }

  public async format(agentsArray: any[]): Promise<AgentDto[]> {
    const formatedArray = agentsArray.map((agent) => {
      return {
        actor: agent.Actor,
        virtualCc: agent.VirtualCC,
        date: this.parseDate(agent.Date),
        totalBreaks: agent.TotalBreaks,
        totalToilettes: agent.TotalToilettes,
        totalTrainings: agent.TotalTrainings,
        totalPersonals: agent.TotalPersonals,
        totalCustoms: agent.TotalCustoms,
        totalPauses: agent.TotalPauses,
        totalLogins: agent.TotalLogins,
        loggedState: agent.LoggedState,
        activeState: agent.ActiveState,
        breakState: agent.BreakState,
        pauseState: agent.PauseState,
        attePauseState: agent.AttePauseState,
        wrapupPauseState: agent.WrapupPauseState,
        trainingState: agent.TrainingState,
        personalState: agent.PersonalState,
        toiletteState: agent.ToiletteState,
        customsState: agent.CustomsState,
        preferredCampaign: agent.PreferredCampaign,
        totalUnavailable: agent.TotalUnavailable,
        unavailableState: agent.UnavailableState,
        atteActiveState: agent.AtteActiveState,
        wrapupActiveState: agent.WrapupActiveState,
        durationCount: agent.DurationCount,
        durationTime: agent.DurationTime,
        answerCount: agent.AnswerCount,
        answerTime: agent.AnswerTime,
        waitCount: agent.WaitCount,
        waitTime: agent.WaitTime,
        controlAgentCount: agent.ControlAgentCount,
        controlAgentTime: agent.ControlAgentTime,
        attentionCount: agent.AttentionCount,
        attentionTime: agent.AttentionTime,
        wrapupCount: agent.WrapupCount,
        wrapupTime: agent.WrapupTime,
        desertionCount: agent.DesertionCount,
        desertionTime: agent.DesertionTime,
        holdCount: agent.HoldCount,
        holdTime: agent.HoldTime,
        transferCount: agent.TransferCount,
        transferTime: agent.TransferTime,
        requeuedCount: agent.RequeuedCount,
        requeuedTime: agent.RequeuedTime,
        ringingCount: agent.RingingCount,
        ringingTime: agent.RingingTime,
        ringBackCount: agent.RingBackCount,
        ringBackTime: agent.RingBackTime,
        previewCount: agent.PreviewCount,
        previewTime: agent.PreviewTime,
      };
    });

    await this.setAgentsCache();

    return formatedArray;
  }

  public async save(agentDto: AgentDto[]): Promise<AgentDto[]> {
    const insertedAgents = agentDto.map((agent) => {
      return this.agentsRepository.saveAgents(agent);
    });

    return Promise.all(insertedAgents);
  }

  private async setAgentsCache() {
    const cachedObject = {
      date: DateTime.now().toFormat('yyyy-MM-dd'),
    };
    await this.cacheManager.set(this.getStrategyName(), JSON.stringify(cachedObject), {
      ttl: 1800,
    });
  }
  private parseDate(date: string): Date {
    if (!date) {
      return null;
    }
    const { timeZone } = this.config;

    return DateTime.fromFormat(date, 'yyyy-MM-dd', {
      zone: timeZone,
    }).toJSDate();
  }
}
