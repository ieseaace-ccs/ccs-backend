import { Repository, EntityRepository } from 'typeorm';

import { AgentDto } from '../models/dtos/agent.dto';
import { AgentsEntity } from '../models/entities/agents.entity';

@EntityRepository(AgentsEntity)
export class AgentsRepository extends Repository<AgentsEntity> {
  public async saveAgents(agent: AgentDto): Promise<any> {
    return this.save(agent);
  }
}
