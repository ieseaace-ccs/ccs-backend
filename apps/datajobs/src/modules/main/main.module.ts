import { Module } from '@nestjs/common';
import { ScheduleModule } from '@nestjs/schedule';

import { ConfigModule } from '../config/config.module';
import { ConfigService } from '@nestjs/config';
import { CoreModule } from '../core/core.module';
import { InconcertModule } from '../inconcert/inconcert.module';
import { options } from '../config/infrastructure/options/config.options';
import { SentryModule } from '@ntegral/nestjs-sentry';
import { UploadModule } from '../upload/upload.module';

@Module({
  imports: [
    ScheduleModule.forRoot(),
    ConfigModule.forRoot(options),
    CoreModule,
    InconcertModule,
    UploadModule,
    SentryModule.forRootAsync({
      imports: [ConfigModule],
      useFactory: async (config: ConfigService) => ({
        dsn: config.get('sentry').sentryDsn,
        environment: config.get('sentry').sentryEnvironment
      }),
      inject: [ConfigService]
    })
  ]
})
export class MainModule {}
