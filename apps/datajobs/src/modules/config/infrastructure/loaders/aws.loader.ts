import { registerAs } from '@nestjs/config';

import { configLoader } from './config.loader';
import { AwsConfigType } from '../types/aws.type';

export const awsConfigLoader = registerAs(
  'aws',
  (): AwsConfigType => configLoader().aws
);
