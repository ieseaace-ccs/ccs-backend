import { InConcertConfigType } from '../types/inconcert.type';
import { CronjobsConfigType } from '../types/cronjobs.type';
import { DatabaseConfigType } from '../types/database.type';
import { ServerConfigType } from '../types/server.type';
import { SentryConfigType } from './../types/sentry.type';
import { ApplicationConfigType } from './../types/application.type';
import { AwsConfigType } from '../types/aws.type';
import { CurrencyConfigType } from '../types/currency.type';

export const configLoader = (): ConfigLoader => ({
  server: {
    host: process.env.INCONCERT_MICROSERVICE_HOST,
    port: parseInt(process.env.INCONCERT_MICROSERVICE_PORT, 10),
    listenerPort: parseInt(process.env.INCONCERT_MICROSERVICE_LISTENER_PORT, 10),
  },
  application: {
    campaigns: process.env.CAMPAIGNS.toString().split(','),
    excludedFormFields: process.env.EXCLUDED_FORM_FIELDS.toString().split(','),
    maxUploadRecords: parseInt(process.env.MAX_UPLOAD_RECORDS),
  },

  database: {
    type: process.env.INCONCERT_DB_TYPE,
    host: process.env.INCONCERT_DB_HOST,
    port: parseInt(process.env.INCONCERT_DB_PORT, 10),
    username: process.env.INCONCERT_DB_USERNAME,
    password: process.env.INCONCERT_DB_PASSWORD,
    database: process.env.INCONCERT_DB_NAME,
    synchronize: process.env.INCONCERT_DB_SYNC === '1',
    autoLoadEntities: true,
    migrationsTableName: 'migrations',
    extra: {
      trustServerCertificate: process.env.INCONCERT_DB_TRUST_CERTIFICATE === '1',
      requestTimeout: 0,
    },
  },
  cronjobs: {
    phoneInteractionsCron: process.env.INCONCERT_CRONJOB_PHONE_INTERACTIONS,
    chatInteractionsCron: process.env.INCONCERT_CRONJOB_CHAT_INTERACTIONS,
    whatsInteractionsCron: process.env.INCONCERT_CRONJOB_WHATSAPP_INTERACTIONS,
    mailInteractionsCron: process.env.INCONCERT_CRONJOB_MAIL_INTERACTIONS,
    agentsCron: process.env.INCONCERT_CRONJOB_AGENTS,
    formsCron: process.env.INCONCERT_CRONJOB_FORMS,
  },
  inConcert: {
    apiKey: process.env.INCONCERT_API_KEY,
    baseUrl: process.env.INCONCERT_BASE_URL,
    interactionsUri: process.env.INCONCERT_INTERACTIONS_URI,
    agentsUri: process.env.INCONCERT_AGENTS_URI,
    pageLimit: parseInt(process.env.INCONCERT_PAGE_LIMIT),
    timeZone: process.env.INCONCERT_TIMEZONE,
    localTimeZone: process.env.LOCAL_TIMEZONE,
    dateFormat: process.env.INCONCERT_DATE_FORMAT,
  },
  sentry: {
    sentryDsn: process.env.SENTRY_DSN,
    sentryEnvironment: process.env.ENVIRONMENT,
  },
  aws: {
    accesKey: process.env.AWS_KEY,
    secretKey: process.env.AWS_SECRET,
    bucket: process.env.AWS_BUCKET,
  },
  currency: {
    url: process.env.CURRENCY_URL,
    key: process.env.CURRENCY_KEY,
  },
});

type ConfigLoader = {
  server: ServerConfigType;
  application: ApplicationConfigType;
  database: DatabaseConfigType;
  cronjobs: CronjobsConfigType;
  inConcert: InConcertConfigType;
  sentry: SentryConfigType;
  aws: AwsConfigType;
  currency: CurrencyConfigType;
};
