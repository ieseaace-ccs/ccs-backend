import { registerAs } from '@nestjs/config';

import { configLoader } from './config.loader';
import { CronjobsConfigType } from '../types/cronjobs.type';

export const cronjobsConfigLoader = registerAs(
  'cronjobs',
  (): CronjobsConfigType => configLoader().cronjobs
);
