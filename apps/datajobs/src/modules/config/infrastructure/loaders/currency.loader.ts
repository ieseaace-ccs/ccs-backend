import { registerAs } from '@nestjs/config';

import { configLoader } from './config.loader';
import { CurrencyConfigType } from '../types/currency.type';

export const currencyConfigLoader = registerAs(
  'currency',
  (): CurrencyConfigType => configLoader().currency,
);
