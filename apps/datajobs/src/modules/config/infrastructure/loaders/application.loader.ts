import { registerAs } from '@nestjs/config';

import { configLoader } from './config.loader';
import { ApplicationConfigType } from './../types/application.type';

export const applicationConfigLoader = registerAs(
  'application',
  (): ApplicationConfigType => configLoader().application
);
