import { registerAs } from '@nestjs/config';

import { configLoader } from './config.loader';
import { InConcertConfigType } from '../types/inconcert.type';

export const inConcertConfigLoader = registerAs(
  'inConcert',
  (): InConcertConfigType => configLoader().inConcert
);
