export type ApplicationConfigType = {
  campaigns: string[];
  excludedFormFields: string[];
  maxUploadRecords: number;
};
