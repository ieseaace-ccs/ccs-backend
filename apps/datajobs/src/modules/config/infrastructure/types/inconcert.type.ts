export type InConcertConfigType = {
  apiKey: string;
  baseUrl: string;
  interactionsUri: string;
  agentsUri: string;
  pageLimit: number;
  timeZone: string;
  localTimeZone: string;
  dateFormat: string;
};
