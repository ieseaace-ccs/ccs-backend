export type AwsConfigType = {
  accesKey: string;
  secretKey: string;
  bucket: string;
};
