export type CurrencyConfigType = {
  url: string;
  key: string;
};
