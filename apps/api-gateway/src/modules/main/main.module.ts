import { Module } from '@nestjs/common';

import { ConfigModule } from '../config/config.module';
import { CoreModule } from '../core/core.module';
import { AuthModule } from '../auth/auth.module';
import { UsersModule } from '../users/users.module';

import { options } from '../config/infrastructure/options/config.options';
import { LayoutsModule } from '../upload/upload.module';
import { ReportsModule } from '../reports/reports.module';
import { JobsModule } from '../jobs/jobs.module';

@Module({
  imports: [
    ConfigModule.forRoot(options),
    CoreModule,
    AuthModule,
    UsersModule,
    LayoutsModule,
    ReportsModule,
    JobsModule
  ]
})
export class MainModule { }
