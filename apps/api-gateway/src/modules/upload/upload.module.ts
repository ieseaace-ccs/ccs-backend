import { Module } from '@nestjs/common';

import { UploadService } from './application/upload.service';
import { UploadController } from './infrastructure/controllers/upload.controller';
import {
  datajobsClientProvider,
  userClientProvider,
  authClientProvider
} from '../core/infrastructure/providers/services.provider';

@Module({
  controllers: [UploadController],
  providers: [
    UploadService,
    datajobsClientProvider,
    userClientProvider,
    authClientProvider
  ]
})
export class LayoutsModule {}
