import { Injectable, Inject, HttpStatus, HttpException } from '@nestjs/common';
import { ClientProxy } from '@nestjs/microservices';

import { UploadResponseDto } from '../../../../../datajobs/src/modules/upload/domain/models/dtos/upload-response.dto';

@Injectable()
export class UploadService {
  constructor(@Inject('DATAJOBS_CLIENT') private readonly client: ClientProxy) {}
  public async uploadLayout(file: Express.Multer.File, user: any): Promise<UploadResponseDto> {
    if (!file) {
      throw new HttpException('No file sent', HttpStatus.BAD_REQUEST);
    }
    try {
      return await this.client.send({ role: 'file', cmd: 'upload' }, { file, user }).toPromise();
    } catch (error) {
      throw new HttpException(error.message, error.statusCode);
    }
  }
}
