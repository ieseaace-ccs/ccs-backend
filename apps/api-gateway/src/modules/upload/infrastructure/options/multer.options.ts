import { HttpException, HttpStatus } from '@nestjs/common';

export const multerOptions = {
  fileFilter: (req, file, cb) => {
    if (
      file.mimetype.includes('excel') ||
      file.mimetype.includes('spreadsheetml')
    ) {
      cb(null, true);
    } else {
      cb(
        new HttpException(
          { message: 'Invalid format, only .xls and .xlsx allowed' },
          HttpStatus.BAD_REQUEST
        ),
        false
      );
    }
  }
};
