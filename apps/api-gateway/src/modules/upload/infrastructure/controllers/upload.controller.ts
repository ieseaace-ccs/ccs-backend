import { Controller, UseGuards, Post, UseInterceptors, UploadedFile, Req } from '@nestjs/common';
import { FileInterceptor } from '@nestjs/platform-express';
import { ApiConsumes, ApiBody, ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { UploadResponseDto } from '../../../../../../datajobs/src/modules/upload/domain/models/dtos/upload-response.dto';
import { Roles } from '../../../core/infrastructure/decorators/roles.decorator';
import { AuthGuard } from '../../../core/infrastructure/guards/auth.guard';
import { RolesGuard } from '../../../core/infrastructure/guards/roles.guard';

import { UploadService } from '../../application/upload.service';
import { multerOptions } from '../options/multer.options';

@ApiBearerAuth()
@ApiTags('Uploads')
@Controller('upload')
export class UploadController {
  constructor(private readonly layoutService: UploadService) {}

  @ApiConsumes('multipart/form-data')
  @ApiBody({
    schema: {
      type: 'object',
      properties: {
        file: {
          type: 'string',
          format: 'binary',
        },
      },
    },
  })
  @UseGuards(AuthGuard, RolesGuard)
  @Roles('admin', 'super')
  @UseInterceptors(FileInterceptor('file', multerOptions))
  @Post('/viajes-palacio')
  public uploadLayout(
    @UploadedFile() file: Express.Multer.File,
    @Req() req: any,
  ): Promise<UploadResponseDto> {
    return this.layoutService.uploadLayout(file, req.user);
  }
}
