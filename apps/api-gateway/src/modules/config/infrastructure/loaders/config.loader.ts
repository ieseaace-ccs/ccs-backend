import {
  ServerConfigTypes,
  SwaggerConfigTypes,
  ServicesConfigType
} from '../types';

export const configLoader = (): ConfigLoader => ({
  server: {
    environment: process.env.ENVIRONMENT,
    port: parseInt(process.env.API_GATEWAY_PORT, 10)
  },
  swagger: {
    active: process.env.API_GATEWAY_SWAGGER_ACTIVE === '1',
    path: process.env.API_GATEWAY_SWAGGER_PATH
  },
  services: {
    auth: {
      host: process.env.AUTH_MICROSERVICE_HOST,
      port: parseInt(process.env.AUTH_MICROSERVICE_PORT)
    },
    users: {
      host: process.env.USERS_MICROSERVICE_HOST,
      port: parseInt(process.env.USERS_MICROSERVICE_PORT)
    },
    datajobs: {
      host: process.env.DATAJOBS_MICROSERVICE_HOST,
      port: parseInt(process.env.DATAJOBS_MICROSERVICE_PORT)
    },
    reports: {
      host: process.env.REPORTS_MICROSERVICE_HOST,
      port: parseInt(process.env.REPORTS_MICROSERVICE_PORT)
    }
  }
});

type ConfigLoader = {
  server: ServerConfigTypes;
  swagger: SwaggerConfigTypes;
  services: ServicesConfigType;
};
