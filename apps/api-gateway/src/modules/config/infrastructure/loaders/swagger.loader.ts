import { registerAs } from '@nestjs/config';

import { configLoader } from './config.loader';
import { SwaggerConfigTypes } from '../types/swagger.type';

export const swaggerConfigLoader = registerAs(
  'swagger',
  (): SwaggerConfigTypes => configLoader().swagger
);
