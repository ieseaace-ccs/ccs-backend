import { registerAs } from '@nestjs/config';

import { configLoader } from './config.loader';
import { ServerConfigTypes } from '../types/server.type';

export const serverConfigLoader = registerAs(
  'server',
  (): ServerConfigTypes => configLoader().server
);
