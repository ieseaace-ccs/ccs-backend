import * as Joi from 'joi';

export const configSchema = Joi.object({
  API_GATEWAY_PORT: Joi.number().required(),
  API_GATEWAY_SWAGGER_ACTIVE: Joi.number().required(),
  API_GATEWAY_SWAGGER_PATH: Joi.string().required(),
  AUTH_MICROSERVICE_HOST: Joi.string().required(),
  AUTH_MICROSERVICE_PORT: Joi.number().required(),
  USERS_MICROSERVICE_HOST: Joi.string().required(),
  USERS_MICROSERVICE_PORT: Joi.number().required(),
  DATAJOBS_MICROSERVICE_HOST: Joi.string().required(),
  DATAJOBS_MICROSERVICE_PORT: Joi.number().required(),
  REPORTS_MICROSERVICE_HOST: Joi.string().required(),
  REPORTS_MICROSERVICE_PORT: Joi.number().required()
});
