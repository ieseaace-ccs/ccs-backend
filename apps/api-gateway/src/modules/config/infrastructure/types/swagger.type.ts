export type SwaggerConfigTypes = {
  active: boolean;
  path: string;
};
