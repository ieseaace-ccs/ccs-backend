export type ServerConfigTypes = {
  port: number;
  environment: string;
};
