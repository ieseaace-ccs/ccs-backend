import { Module } from '@nestjs/common';

import { ReportsService } from './application/services/reports.service';
import { ReportsController } from './infrastructure/controllers/reports.controller';
import {
  authClientProvider,
  reportsClientProvider
} from '../core/infrastructure/providers/services.provider';

@Module({
  controllers: [ReportsController],
  providers: [ReportsService, authClientProvider, reportsClientProvider]
})
export class ReportsModule {}
