export enum Channels {
  PHONE = 'phone',
  CHAT = 'chat',
  WHATSAPP = 'whatsapp',
  MAIL = 'mail'
}
