export enum OutputFormats {
  JSON = 'json',
  HTML = 'html'
}
