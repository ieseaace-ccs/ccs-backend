import {
  Controller,
  Get,
  UseGuards,
  Query,
  Param,
  HttpException,
  Post,
  Body
} from '@nestjs/common';
import { ApiBearerAuth, ApiParam, ApiResponse, ApiTags } from '@nestjs/swagger';

import { ReportsService } from '../../application/services/reports.service';
import { Roles } from '../../../core/infrastructure/decorators/roles.decorator';
import { AuthGuard } from '../../../core/infrastructure/guards/auth.guard';
import { RolesGuard } from '../../../core/infrastructure/guards/roles.guard';
import { InConcertQueryParams } from '../dtos/inconcert-query-params.dto';
import { PalacioLayoutQueryParams } from '../dtos/vp-layout-query-params.dto';
import { CampaignAccumulatorsPhoneDto } from '../dtos/phone-campaign-accumulators.dto';
import { Channels } from '../enums/channels.enum';
import { PalacioAccumulatorsDto } from '../dtos/palacio-accumulators.dto';
import { ObjetivesQueryParams } from '../dtos/objetives-query-params.dto';
import { ObjetivesDto } from '../dtos/set-objetives-params.dto';

const campaignAccumulatorsResponse = {
  type: CampaignAccumulatorsPhoneDto,
  description: 'Returns campaign accumulators report',
  isArray: true,
  status: 200
};
const palacioAccumulatorsResponse = {
  type: PalacioAccumulatorsDto,
  description: 'Returns Palacio de Hierro accumulators report',
  isArray: true,
  status: 200
};
@ApiBearerAuth()
@ApiTags('Reports')
@UseGuards(AuthGuard, RolesGuard)
@Roles('admin', 'super', 'external')
@Controller('reports')
export class ReportsController {
  constructor(private readonly reportsService: ReportsService) {}

  @ApiResponse(campaignAccumulatorsResponse)
  @ApiParam({ name: 'channel', enum: Channels })
  @Get('/accumulators/campaigns/:channel')
  public campaignsAccumulatorsPhone(
    @Query() query: InConcertQueryParams,
    @Param('channel') channel: Channels
  ): Promise<CampaignAccumulatorsPhoneDto[]> {
    if (!Object.values(Channels).includes(channel)) {
      throw new HttpException('Invalid channel', 400);
    }
    return this.reportsService.campaignAccumulators(query, channel);
  }
  @ApiResponse(palacioAccumulatorsResponse)
  @Get('/accumulators/viajes-palacio')
  public viajesPalacioLayout(
    @Query() query: PalacioLayoutQueryParams
  ): Promise<PalacioAccumulatorsDto[]> {
    return this.reportsService.viajesPalacioAccumulators(query);
  }

  @Get('/accumulators/viajes-palacio/conversion')
  public viajesPalacioConversion(
    @Query() query: PalacioLayoutQueryParams
  ): Promise<any[]> {
    return this.reportsService.viajesPalacioConversion(query);
  }

  @Get('/objetives')
  public getObjetives(@Query() query: ObjetivesQueryParams): Promise<any> {
    return this.reportsService.getObjetives(query);
  }

  @Roles('admin')
  @Post('/objetives')
  public setObjetives(@Body() body: ObjetivesDto): Promise<any> {
    return this.reportsService.setObjetives(body);
  }
}
