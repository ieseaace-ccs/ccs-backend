import {
  IsDefined,
  IsNotEmpty,
  IsString,
  IsNumber,
  Min,
  Max,
  IsEnum,
  IsDateString
} from 'class-validator';
import { Type } from 'class-transformer';
import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';
import { OutputFormats } from '../enums/formats.enum';

export class InConcertQueryParams {
  @ApiProperty()
  @IsNumber()
  @Type(() => Number)
  @Min(0)
  @Max(4)
  interval: number;

  @ApiProperty()
  @IsNumber()
  @Type(() => Number)
  @Min(0)
  @Max(3)
  groupBy: number;

  @ApiProperty()
  @IsNumber()
  @Type(() => Number)
  @Min(0)
  @Max(1)
  totalized: number;

  @ApiProperty()
  @IsString()
  @IsDefined()
  @IsNotEmpty()
  campaign: string;

  @ApiProperty()
  @IsString()
  @IsDefined()
  @IsNotEmpty()
  skill: string;

  @ApiProperty({ enum: OutputFormats })
  @IsString()
  @IsEnum(OutputFormats)
  format: OutputFormats;

  @ApiPropertyOptional()
  @IsDateString()
  @IsString()
  startDate: string;

  @ApiPropertyOptional()
  @IsDateString()
  @IsString()
  endDate: string;
}
