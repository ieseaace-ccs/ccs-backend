import { ApiProperty } from '@nestjs/swagger';

export class PalacioAccumulatorsDto {
  @ApiProperty()
  fecha: Date | null;

  @ApiProperty()
  capturadas: number;

  @ApiProperty()
  vendidas: number;

  @ApiProperty()
  monto_capturado: number;

  @ApiProperty()
  monto_vendido: number;

  @ApiProperty()
  ticket_promedio: number;
}
