import { IsString, IsDefined } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class ObjetivesQueryParams {
  @ApiProperty()
  @IsString()
  @IsDefined()
  campaign: string;

  @ApiProperty()
  @IsString()
  @IsDefined()
  indicator: string;
}
