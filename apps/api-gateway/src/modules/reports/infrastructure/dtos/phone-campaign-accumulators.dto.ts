import { ApiProperty } from '@nestjs/swagger';

export class CampaignAccumulatorsPhoneDto {
  @ApiProperty()
  Campania: string | null;

  @ApiProperty()
  Skill: string | null;

  @ApiProperty()
  Fecha: Date | null;

  @ApiProperty()
  Recibidas: number;

  @ApiProperty()
  Atendidas: number;

  @ApiProperty()
  Abandonadas: number;

  @ApiProperty()
  ['Ate<20']: number;

  @ApiProperty()
  ['Ate>20']: number;

  @ApiProperty()
  A1: number;

  @ApiProperty()
  A2: number;

  @ApiProperty()
  A3: number;

  @ApiProperty()
  A4: number;

  @ApiProperty()
  ['Aba<5']: number;

  @ApiProperty()
  ['Aba>5']: number;

  @ApiProperty()
  Salientes: number;

  @ApiProperty()
  ['Salientes Atendidas']: number;

  @ApiProperty()
  ['AHT Inbound']: string;

  @ApiProperty()
  ['AHT Outbound']: string;

  @ApiProperty()
  ['ATT Inbound']: string;

  @ApiProperty()
  ['ATT Outboound']: string;

  @ApiProperty()
  ASA: string;

  @ApiProperty()
  AAT: string;
}
