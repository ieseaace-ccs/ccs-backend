import { IsString, IsDefined, IsNumber } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class ObjetivesDto {
  @ApiProperty()
  @IsString()
  @IsDefined()
  campaign: string;

  @ApiProperty()
  @IsString()
  @IsDefined()
  indicator: string;

  @ApiProperty()
  @IsNumber()
  @IsDefined()
  value: number;
}
