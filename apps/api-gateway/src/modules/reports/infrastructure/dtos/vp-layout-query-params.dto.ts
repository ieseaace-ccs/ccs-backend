import { IsString, IsNumber, Min, Max, IsDateString } from 'class-validator';
import { Type } from 'class-transformer';
import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';

export class PalacioLayoutQueryParams {
  @ApiProperty()
  @IsNumber()
  @Type(() => Number)
  @Min(0)
  @Max(4)
  interval: number;

  @ApiProperty()
  @IsNumber()
  @Type(() => Number)
  @Min(0)
  @Max(3)
  groupBy: number;

  @ApiPropertyOptional()
  @IsDateString()
  @IsString()
  startDate: string;

  @ApiPropertyOptional()
  @IsDateString()
  @IsString()
  endDate: string;
}
