import { Injectable, Inject, HttpException } from '@nestjs/common';
import { ClientProxy } from '@nestjs/microservices';
import { InConcertQueryParams } from '../../infrastructure/dtos/inconcert-query-params.dto';
import { ObjetivesQueryParams } from '../../infrastructure/dtos/objetives-query-params.dto';
import { ObjetivesDto } from '../../infrastructure/dtos/set-objetives-params.dto';
import { PalacioLayoutQueryParams } from '../../infrastructure/dtos/vp-layout-query-params.dto';

@Injectable()
export class ReportsService {
  constructor(@Inject('REPORTS_CLIENT') private readonly client: ClientProxy) {}

  public async campaignAccumulators(
    params: InConcertQueryParams,
    channel: any
  ): Promise<any> {
    try {
      return await this.client
        .send(
          { role: 'reports', reports: 'campaignAccumulators' },
          { ...params, channel }
        )
        .toPromise();
    } catch (error) {
      throw new HttpException(error.message, error.statusCode);
    }
  }
  public async viajesPalacioAccumulators(
    params: PalacioLayoutQueryParams
  ): Promise<any> {
    try {
      return await this.client
        .send({ role: 'reports', reports: 'palacioAccumulators' }, params)
        .toPromise();
    } catch (error) {
      throw new HttpException(error.message, error.statusCode);
    }
  }

  public async viajesPalacioConversion(
    params: PalacioLayoutQueryParams
  ): Promise<any> {
    try {
      return await this.client
        .send({ role: 'reports', reports: 'palacioConversion' }, params)
        .toPromise();
    } catch (error) {
      throw new HttpException(error.message, error.statusCode);
    }
  }
  public async getObjetives(params: ObjetivesQueryParams): Promise<any> {
    try {
      return await this.client
        .send({ role: 'reports', reports: 'getObjetives' }, params)
        .toPromise();
    } catch (error) {
      throw new HttpException(error.message, error.statusCode);
    }
  }
  public async setObjetives(body: ObjetivesDto): Promise<any> {
    try {
      return await this.client
        .send({ role: 'reports', reports: 'setObjetives' }, body)
        .toPromise();
    } catch (error) {
      throw new HttpException(error.message, error.statusCode);
    }
  }
}
