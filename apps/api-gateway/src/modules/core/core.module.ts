import {
  authClientProvider,
  datajobsClientProvider,
  userClientProvider,
  reportsClientProvider
} from './infrastructure/providers/services.provider';
import { Module } from '@nestjs/common';
import { APP_FILTER, APP_INTERCEPTOR } from '@nestjs/core';

import { TransformInterceptor } from './infrastructure/interceptors/transform.interceptor';
import { HttpExceptionFilter } from './infrastructure/filters/http-exception.filter';

@Module({
  providers: [
    { provide: 'AUTH_CLIENT', useValue: authClientProvider },
    { provide: 'USER_CLIENT', useValue: userClientProvider },
    { provide: 'DATAJOBS_CLIENT', useValue: datajobsClientProvider },
    { provide: 'REPORTS_CLIENT', useValue: reportsClientProvider },
    {
      provide: APP_FILTER,
      useClass: HttpExceptionFilter
    },
    {
      provide: APP_INTERCEPTOR,
      useClass: TransformInterceptor
    }
  ]
})
export class CoreModule {}
