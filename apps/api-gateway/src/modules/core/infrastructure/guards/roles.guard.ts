import { Injectable, CanActivate, ExecutionContext } from '@nestjs/common';
import { Reflector } from '@nestjs/core';
import jwtDecode from 'jwt-decode';

@Injectable()
export class RolesGuard implements CanActivate {
  constructor(private readonly reflector: Reflector) {}

  canActivate(context: ExecutionContext): boolean {
    const roles = this.reflector.get<string[]>('roles', context.getHandler());
    if (!roles) {
      return true;
    }
    // TODO: Handle malformed token errors
    const req = context.switchToHttp().getRequest();
    const jwt = req.headers['authorization']?.split(' ')[1];
    const token: any = jwtDecode(jwt);
    const user = token.user;

    req.user = token.user;

    return this.checkRoles(roles, user.roles);
  }

  private checkRoles(routeRoles: string[], userRoles: string[]): boolean {
    return routeRoles.some((role) => userRoles.includes(role));
  }
}
