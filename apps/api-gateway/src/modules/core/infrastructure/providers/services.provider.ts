import { ClientProxyFactory, Transport } from '@nestjs/microservices';
import { ConfigService } from '@nestjs/config';

export const authClientProvider = {
  provide: 'AUTH_CLIENT',
  inject: [ConfigService],
  useFactory: (config: ConfigService) =>
    ClientProxyFactory.create({
      transport: Transport.TCP,
      options: {
        host: config.get('services').auth.host,
        port: config.get('services').auth.port
      }
    })
};

export const userClientProvider = {
  provide: 'USER_CLIENT',
  inject: [ConfigService],
  useFactory: (config: ConfigService) =>
    ClientProxyFactory.create({
      transport: Transport.TCP,
      options: {
        host: config.get('services').users.host,
        port: config.get('services').users.port
      }
    })
};

export const datajobsClientProvider = {
  provide: 'DATAJOBS_CLIENT',
  inject: [ConfigService],
  useFactory: (config: ConfigService) =>
    ClientProxyFactory.create({
      transport: Transport.TCP,
      options: {
        host: config.get('services').datajobs.host,
        port: config.get('services').datajobs.port
      }
    })
};

export const reportsClientProvider = {
  provide: 'REPORTS_CLIENT',
  inject: [ConfigService],
  useFactory: (config: ConfigService) =>
    ClientProxyFactory.create({
      transport: Transport.TCP,
      options: {
        host: config.get('services').reports.host,
        port: config.get('services').reports.port
      }
    })
};
