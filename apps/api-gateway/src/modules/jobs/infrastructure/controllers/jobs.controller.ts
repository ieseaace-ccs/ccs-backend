import { Controller, Get, Inject, Injectable, Param, Query } from '@nestjs/common';
import { ClientProxy } from '@nestjs/microservices';

export class BulkUpsertDto {
  startDate: string;
  endDate: string;
  strategy: string;
}

@Injectable()
@Controller('jobs')
export class JobsController {
  constructor(@Inject('DATAJOBS_CLIENT') private readonly client: ClientProxy) {}

  @Get('/:strategy')
  async doJob(@Param('strategy') strategy: string, @Query() query: any): Promise<any> {
    const dto = new BulkUpsertDto();
    dto.strategy = strategy;
    dto.startDate = query.startDate;
    dto.endDate = query.endDate;

    return this.client.send({ cmd: 'jobs' }, dto);
  }
}
