import { Module } from '@nestjs/common';
import { datajobsClientProvider } from '../core/infrastructure/providers/services.provider';
import { JobsController } from './infrastructure/controllers/jobs.controller';

@Module({
  controllers: [JobsController],
  providers: [datajobsClientProvider],
})
export class JobsModule {}
