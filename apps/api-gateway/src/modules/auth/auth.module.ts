import { Module } from '@nestjs/common';

import { AuthService } from './domain/services/auth.service';
import { AuthController } from './infrastructure/controllers/auth.controller';
import { authClientProvider } from '../core/infrastructure/providers/services.provider';

@Module({
  controllers: [AuthController],
  providers: [AuthService, authClientProvider]
})
export class AuthModule {}
