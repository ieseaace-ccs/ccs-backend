import {
  Controller,
  Post,
  Body,
  HttpException,
  HttpCode,
  HttpStatus
} from '@nestjs/common';
import { ApiResponse, ApiTags } from '@nestjs/swagger';

import { AuthService } from '../../domain/services/auth.service';
import { ChangePassDto } from '../dtos/change-password.dto';
import { LoginResponseDto } from '../dtos/login-response';
import { LoginDto } from './../dtos/login.dto';

const authResponse = {
  status: 200,
  type: LoginResponseDto,
  description: 'Returns auth token'
};
@ApiTags('Auth')
@Controller()
export class AuthController {
  constructor(private readonly authService: AuthService) {}

  @Post('auth')
  @ApiResponse(authResponse)
  @HttpCode(HttpStatus.OK)
  async login(@Body() data: LoginDto): Promise<LoginResponseDto> {
    try {
      return await this.authService.login(data);
    } catch (error) {
      throw new HttpException(error.message, error.statusCode || 500);
    }
  }
  @Post('auth/change-password')
  @ApiResponse(authResponse)
  @HttpCode(HttpStatus.NO_CONTENT)
  async change(@Body() data: ChangePassDto): Promise<void> {
    try {
      return await this.authService.changePassword(data);
    } catch (error) {
      throw new HttpException(error.message, error.statusCode || 500);
    }
  }
}
