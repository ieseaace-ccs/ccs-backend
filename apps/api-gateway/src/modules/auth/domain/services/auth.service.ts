import { Injectable, Inject } from '@nestjs/common';
import { ClientProxy } from '@nestjs/microservices';

import { LoginDto } from '../../infrastructure/dtos/login.dto';
import { LoginResponseDto } from '../../infrastructure/dtos/login-response';

@Injectable()
export class AuthService {
  constructor(@Inject('AUTH_CLIENT') private readonly client: ClientProxy) {}
  public login(data: LoginDto): Promise<LoginResponseDto> {
    return this.client.send({ role: 'auth', cmd: 'login' }, data).toPromise();
  }
  public changePassword(data: LoginDto): Promise<void> {
    return this.client.send({ role: 'auth', cmd: 'change' }, data).toPromise();
  }
}
