import { ApiProperty } from '@nestjs/swagger';
import { Expose, Exclude, Transform } from 'class-transformer';
import { IsUUID, IsString, IsNumber, IsDate } from 'class-validator';

@Exclude()
export class UsersResponseDto {
  @ApiProperty()
  @Expose()
  @IsUUID()
  @IsString()
  uuid: string;

  @ApiProperty()
  @Expose()
  @IsString()
  username: string;

  @ApiProperty()
  @Expose()
  @IsString()
  name: string;

  @ApiProperty()
  @Expose()
  @IsString()
  lastname: string;

  @ApiProperty()
  @Expose()
  @IsString()
  secondLastname: string;

  @ApiProperty()
  @Expose()
  @IsString()
  mainEmail: string;

  @ApiProperty()
  @Expose()
  @IsNumber()
  status: number;

  @ApiProperty()
  @Expose()
  @IsNumber()
  process: number;

  @ApiProperty()
  @Expose()
  @IsDate()
  createdAt: Date;
  @ApiProperty()
  @Expose()
  @Transform(({ value }) => {
    if (!value) {
      return [];
    }
    return value.map((roleObject: any) => {
      return roleObject.roleName;
    });
  })
  roles: string[];
}
