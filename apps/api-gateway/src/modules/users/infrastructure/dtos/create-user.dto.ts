import { ApiProperty } from '@nestjs/swagger';
import { IsArray, IsEmail, IsString } from 'class-validator';

export class CreateUserDto {
  @ApiProperty()
  @IsString()
  name: string;

  @ApiProperty()
  @IsString()
  lastname: string;

  @ApiProperty()
  @IsString()
  secondLastname: string;

  @ApiProperty()
  @IsString()
  @IsEmail()
  mainEmail: string;
  @ApiProperty()
  @IsArray()
  roles: string[];
}
