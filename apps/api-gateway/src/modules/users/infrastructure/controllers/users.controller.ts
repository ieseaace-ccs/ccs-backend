import { Controller, Get, UseGuards, Param, Post, Body } from '@nestjs/common';

import { UsersService } from '../../application/services/users.service';
import { Roles } from '../../../core/infrastructure/decorators/roles.decorator';
import { AuthGuard } from '../../../core/infrastructure/guards/auth.guard';
import { RolesGuard } from '../../../core/infrastructure/guards/roles.guard';
import { UsersResponseDto } from '../dtos/user-response.dto';
import { CreateUserDto } from '../dtos/create-user.dto';
import { ApiBearerAuth, ApiTags, ApiResponse } from '@nestjs/swagger';

const userResponse = {
  type: UsersResponseDto,
  description: 'Returns User Entity'
};
@ApiBearerAuth()
@ApiTags('Users')
@Controller('user')
export class UsersController {
  constructor(private readonly usersService: UsersService) {}

  @UseGuards(AuthGuard, RolesGuard)
  @Roles('admin', 'super')
  @ApiResponse({ ...userResponse, status: 200 })
  @Get('/:id')
  public getUser(@Param('id') id: string): Promise<UsersResponseDto> {
    return this.usersService.getUser(id);
  }
  @UseGuards(AuthGuard, RolesGuard)
  @Roles('admin', 'super')
  @ApiResponse({ ...userResponse, status: 201 })
  @Post('/')
  public createtUser(@Body() user: CreateUserDto): Promise<UsersResponseDto> {
    return this.usersService.createUser(user);
  }
}
