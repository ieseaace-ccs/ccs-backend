import { Injectable, Inject, HttpException } from '@nestjs/common';
import { plainToClass } from 'class-transformer';
import { ClientProxy } from '@nestjs/microservices';

import { UsersResponseDto } from '../../infrastructure/dtos/user-response.dto';
import { CreateUserDto } from '../../infrastructure/dtos/create-user.dto';

@Injectable()
export class UsersService {
  constructor(@Inject('USER_CLIENT') private readonly client: ClientProxy) {}

  public async getUser(id: string): Promise<UsersResponseDto> {
    try {
      const response = await this.client
        .send(
          { role: 'user', cmd: 'get' },
          { where: { uuid: id }, relations: ['roles'] }
        )
        .toPromise();

      return plainToClass(UsersResponseDto, response);
    } catch (error) {
      throw new HttpException(error.message, error.statusCode);
    }
  }

  public async createUser(user: CreateUserDto): Promise<UsersResponseDto> {
    try {
      const response = await this.client
        .send({ role: 'user', cmd: 'create' }, user)
        .toPromise();

      return plainToClass(UsersResponseDto, response);
    } catch (error) {
      throw new HttpException(error.message, error.statusCode);
    }
  }
}
