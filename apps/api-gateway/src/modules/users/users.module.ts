import { Module } from '@nestjs/common';

import { UsersService } from './application/services/users.service';
import { UsersController } from './infrastructure/controllers/users.controller';
import {
  authClientProvider,
  userClientProvider
} from '../core/infrastructure/providers/services.provider';

@Module({
  controllers: [UsersController],
  providers: [UsersService, authClientProvider, userClientProvider]
})
export class UsersModule {}
