import { NestFactory } from '@nestjs/core';
import { ConfigService } from '@nestjs/config';
import { SwaggerModule, DocumentBuilder } from '@nestjs/swagger';
import { Logger, ValidationPipe } from '@nestjs/common';
import { MainModule } from './modules/main/main.module';
import {
  ServerConfigTypes,
  SwaggerConfigTypes
} from './modules/config/infrastructure/types';

export async function bootstrap(): Promise<void> {
  const app = await NestFactory.create(MainModule);

  app.useGlobalPipes(new ValidationPipe());

  const configService: ConfigService = app.get('ConfigService');

  const swaggerConfig = configService.get<SwaggerConfigTypes>('swagger');
  if (swaggerConfig.active) {
    const documentBuild = new DocumentBuilder()
      .setTitle('Binomia API Gateway')
      .setDescription('API Gateway for Binomia backend')
      .setVersion('0.0.1')
      .addBearerAuth()
      .build();
    const document = SwaggerModule.createDocument(app, documentBuild);
    SwaggerModule.setup(swaggerConfig.path, app, document);
  }

  const { port } = configService.get<ServerConfigTypes>('server');

  app.enableCors();

  app.listen(port, () => Logger.log(`Running on port ${port}`, 'API Gateway'));
}

bootstrap();
