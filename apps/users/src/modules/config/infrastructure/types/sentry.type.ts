export type SentryConfigType = {
  sentryDsn: string;
  sentryEnvironment: string;
};
