import * as Joi from 'joi';

export const configSchema = Joi.object({
  SENTRY_DSN: Joi.string().required(),
  USERS_MICROSERVICE_HOST: Joi.string().default('localhost'),
  USERS_MICROSERVICE_PORT: Joi.number().required(),
  USERS_DB_TYPE: Joi.string().required(),
  USERS_DB_HOST: Joi.string().required(),
  USERS_DB_PORT: Joi.number().required(),
  USERS_DB_USERNAME: Joi.string().required(),
  USERS_DB_PASSWORD: Joi.string(),
  USERS_DB_NAME: Joi.string().required(),
  USERS_DB_RUN_MIGRATIONS: Joi.number(),
  USERS_DB_SYNC: Joi.number(),
  USERS_DB_TRUST_CERTIFICATE: Joi.number().required(),
  AUTH_MICROSERVICE_HOST: Joi.string().default('localhost'),
  AUTH_MICROSERVICE_PORT: Joi.number().required()
});
