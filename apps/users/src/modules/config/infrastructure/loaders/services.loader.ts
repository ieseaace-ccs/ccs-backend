import { registerAs } from '@nestjs/config';

import { configLoader } from './config.loader';
import { ServicesConfigType } from '../types/services.type';

export const servicesConfigLoader = registerAs(
  'services',
  (): ServicesConfigType => configLoader().services
);
