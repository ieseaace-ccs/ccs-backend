import { DatabaseConfigType } from '../types/database.type';
import { ServerConfigType } from '../types/server.type';
import { SentryConfigType } from './../types/sentry.type';
import { ServicesConfigType } from '../types/services.type';

export const configLoader = (): ConfigLoader => ({
  server: {
    host: process.env.USERS_MICROSERVICE_HOST,
    port: parseInt(process.env.USERS_MICROSERVICE_PORT, 10)
  },
  database: {
    type: process.env.USERS_DB_TYPE,
    host: process.env.USERS_DB_HOST,
    port: parseInt(process.env.USERS_DB_PORT, 10),
    username: process.env.USERS_DB_USERNAME,
    password: process.env.USERS_DB_PASSWORD,
    database: process.env.USERS_DB_NAME,
    synchronize: process.env.USERS_DB_SYNC === '1',
    autoLoadEntities: true,
    migrationsTableName: 'migrations',
    extra: {
      trustServerCertificate: process.env.USERS_DB_TRUST_CERTIFICATE === '1',
      requestTimeout: 0
    }
  },
  sentry: {
    sentryDsn: process.env.SENTRY_DSN,
    sentryEnvironment: process.env.ENVIRONMENT
  },
  services: {
    auth: {
      host: process.env.AUTH_MICROSERVICE_HOST,
      port: process.env.AUTH_MICROSERVICE_PORT
    }
  }
});

type ConfigLoader = {
  server: ServerConfigType;
  database: DatabaseConfigType;
  sentry: SentryConfigType;
  services: ServicesConfigType;
};
