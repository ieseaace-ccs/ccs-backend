import { ClientProxyFactory, Transport } from '@nestjs/microservices';
import { ConfigService } from '@nestjs/config';

export const authClientProvider = {
  provide: 'AUTH_CLIENT',
  inject: [ConfigService],
  useFactory: (config: ConfigService) =>
    ClientProxyFactory.create({
      transport: Transport.TCP,
      options: {
        host: config.get('services').auth.host,
        port: config.get('services').auth.port
      }
    })
};
