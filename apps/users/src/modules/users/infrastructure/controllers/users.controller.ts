import { Controller } from '@nestjs/common';
import { MessagePattern } from '@nestjs/microservices';

import { UserService } from '../../domain/services/user.service';
import { UserEntity } from './../../domain/models/entities/user.entity';
@Controller()
export class UserController {
  constructor(private readonly userService: UserService) {}

  @MessagePattern({ role: 'user', cmd: 'get' })
  getUser(data: any): Promise<UserEntity> {
    const { options, ...query } = data;
    return this.userService.findOne(query, options);
  }

  @MessagePattern({ role: 'user', cmd: 'create' })
  createUser(user: UserEntity): Promise<UserEntity> {
    return this.userService.createUser(user);
  }

  @MessagePattern({ role: 'user', cmd: 'change-pass' })
  changePass(data: any): Promise<UserEntity> {
    const { uuid, newPassword } = data;
    return this.userService.changePassword(uuid, newPassword);
  }
}
