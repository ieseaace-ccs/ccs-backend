import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { RolesRepository } from './domain/repositories/role.repository';

import { UserRepository } from './domain/repositories/user.repository';
import { UserService } from './domain/services/user.service';
import { UserController } from './infrastructure/controllers/users.controller';
import { authClientProvider } from './infrastructure/providers/services.provider';

@Module({
  imports: [TypeOrmModule.forFeature([UserRepository, RolesRepository])],
  providers: [UserService, authClientProvider],
  controllers: [UserController]
})
export class UsersModule {}
