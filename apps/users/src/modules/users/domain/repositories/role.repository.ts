import { Repository, EntityRepository } from 'typeorm';

import { RolesEntity } from '../models/entities/roles.entity';

@EntityRepository(RolesEntity)
export class RolesRepository extends Repository<RolesEntity> {}
