import { plainToClass } from 'class-transformer';
import { Repository, EntityRepository, FindConditions } from 'typeorm';

import { UserEntity } from '../models/entities/user.entity';

@EntityRepository(UserEntity)
export class UserRepository extends Repository<UserEntity> {
  public findUser(
    query: FindConditions<UserEntity>,
    options?: any
  ): Promise<UserEntity> {
    return this.findOne(query, options);
  }
  public async createUser(user: any): Promise<UserEntity> {
    const userToSave: UserEntity = plainToClass(UserEntity, user);
    return this.save(userToSave);
  }
}
