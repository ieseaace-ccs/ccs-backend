import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { RpcException } from '@nestjs/microservices';

import { UserRepository } from './../repositories/user.repository';
import { UserEntity } from './../models/entities/user.entity';
import { RolesRepository } from '../repositories/role.repository';
import { RolesEntity } from '../models/entities/roles.entity';
@Injectable()
export class UserService {
  constructor(
    @InjectRepository(UserRepository)
    private readonly userRepository: UserRepository,
    @InjectRepository(RolesRepository)
    private readonly rolesRepository: RolesRepository
  ) {}

  public async findOne(query: any, options?: any): Promise<UserEntity> {
    try {
      return await this.userRepository.findUser(query, options);
    } catch (error) {
      throw new RpcException({ message: 'Not Found', statusCode: '404' });
    }
  }
  public async changePassword(uuid: string, pass: string): Promise<UserEntity> {
    try {
      const user = await this.userRepository.findOne({
        where: { uuid }
      });

      user.password = pass;

      return user.save();
    } catch (error) {
      throw new RpcException({ message: 'Not Found', statusCode: '404' });
    }
  }

  public async getRolesByName(roles: RolesEntity[]) {
    const rolesEntity = roles.map((roleName) => {
      return this.rolesRepository.findOneOrFail({
        where: { roleName }
      });
    });
    return Promise.all(rolesEntity);
  }
  public async createUser(user: UserEntity): Promise<UserEntity> {
    try {
      const roles = await this.getRolesByName(user.roles);
      user.roles = roles;
      return await this.userRepository.createUser(user);
    } catch (error) {
      if (error.name === 'EntityNotFound') {
        throw new RpcException({
          message: 'Some role names are invalid',
          statusCode: '409'
        });
      }

      //TODO: Exception Handlers
      throw new RpcException({
        message: 'User already exist',
        statusCode: '409'
      });
    }
  }
}
