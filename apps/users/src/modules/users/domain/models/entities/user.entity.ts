import {
  Entity,
  Column,
  BeforeInsert,
  Unique,
  ManyToMany,
  JoinTable,
  BeforeUpdate
} from 'typeorm';
import { hash } from 'bcrypt';
import { IsEmail, Min } from 'class-validator';

import { CoreEntity } from '../../../../core/domain/entities/base.entity';
import { RolesEntity } from './roles.entity';
@Entity({ name: 'users' })
@Unique(['username'])
@Unique(['mainEmail'])
export class UserEntity extends CoreEntity {
  @Column({ default: 0 })
  status: number;
  @Column({ default: 0 })
  process: number;
  @Column()
  username: string;

  @Column()
  @Min(8)
  password: string;

  @Column()
  name: string;
  @Column()
  lastname: string;
  @Column()
  secondLastname: string;

  @Column()
  @IsEmail()
  mainEmail: string;

  @ManyToMany(() => RolesEntity, (role) => role.id, { cascade: true })
  @JoinTable({
    name: 'user_role',
    joinColumns: [{ name: 'user_id' }],
    inverseJoinColumns: [{ name: 'role_id' }]
  })
  roles: RolesEntity[];

  @BeforeInsert()
  generateUsername() {
    const idCcs =
      this.name.replace(' ', '').substr(0, 1) +
      this.lastname.replace(' ', '').substr(0, 4) +
      this.secondLastname.replace(' ', '').substr(0, 3);
    this.username = idCcs.toLowerCase() + this.sumString(this.secondLastname);
  }

  @BeforeInsert()
  async hashUsername() {
    this.password = await hash(this.username, 10);
  }
  @BeforeUpdate()
  async hashPassword() {
    this.password = await hash(this.password, 10);
  }

  @BeforeInsert()
  async uppercaseNames() {
    this.name = this.name.toUpperCase();
    this.lastname = this.lastname.toUpperCase();
    this.secondLastname = this.secondLastname.toUpperCase();
  }

  private readonly sumString = (string) => {
    return string
      .toLowerCase()
      .split('')
      .reduce((acc, curr) => acc + curr.charCodeAt(0) - 96, 0);
  };
}
