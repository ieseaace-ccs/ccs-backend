import { Entity, Column, PrimaryGeneratedColumn } from 'typeorm';

@Entity({ name: 'roles' })
export class RolesEntity {
  @PrimaryGeneratedColumn('increment')
  id: number;

  @Column({ name: 'role_name' })
  roleName: string;
}
