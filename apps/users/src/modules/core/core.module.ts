import { Module } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { TypeOrmModule, TypeOrmModuleAsyncOptions } from '@nestjs/typeorm';

import { RolesEntity } from './../users/domain/models/entities/roles.entity';
import { UserEntity } from './../users/domain/models/entities/user.entity';
import { DatabaseConfigType } from '../config/infrastructure/types/database.type';

@Module({
  imports: [
    TypeOrmModule.forRootAsync({
      inject: [ConfigService],
      useFactory: (configService: ConfigService) => {
        const config = configService.get<DatabaseConfigType>('database');
        return {
          type: config.type,
          host: config.host,
          port: config.port,
          username: config.username,
          password: config.password,
          database: config.database,
          synchronize: config.synchronize,
          autoLoadEntities: config.autoLoadEntities,
          migrationsTableName: config.migrationsTableName,
          extra: config.extra,
          cli: {
            migrationsDir: 'apps/users/database/migration'
          },
          entities: [UserEntity, RolesEntity]
        } as TypeOrmModuleAsyncOptions;
      }
    })
  ]
})
export class CoreModule {}
