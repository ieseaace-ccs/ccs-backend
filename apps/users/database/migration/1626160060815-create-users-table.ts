import { MigrationInterface, QueryRunner } from 'typeorm';

export class createUsersTable1626160060815 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`CREATE TABLE dbo.users
    (
        uuid uniqueidentifier constraint DF_951b8f1dfc94ac1d0301a14b7e1 default newsequentialid() not null
            constraint PK_951b8f1dfc94ac1d0301a14b7e1
                primary key,
        status int constraint DF_3676155292d72c67cd4e090514f default 0 not null,
        process int constraint DF_818ce194251272ce62703c3c0ec default 0 not null,
        username nvarchar(255) not null
            constraint UQ_fe0bb3f6520ee0469504521e710
                unique,
        password nvarchar(255) not null,
        name nvarchar(255) not null,
        lastname nvarchar(255) not null,
        secondLastname nvarchar(255) not null,
        mainEmail nvarchar(255) not null
            constraint UQ_703d5a3265439b8c35397e9099d
                unique,
        created_at datetime2 constraint DF_c9b5b525a96ddc2c5647d7f7fa5 default getdate() not null,
        updated_at datetime2 constraint DF_6d596d799f9cb9dac6f7bf7c23c default getdate() not null,
        deleted_at datetime2        
    )`);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`DROP TABLE dbo.users`);
  }
}
