import { MigrationInterface, QueryRunner } from 'typeorm';

export class createRolesTable1626372029352 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`CREATE TABLE dbo.roles
    (
        id int identity
            constraint PK_c1433d71a4838793a49dcad46ab
                primary key,
        role_name nvarchar(255) not null
    )`);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`DROP TABLE dbo.roles`);
  }
}
