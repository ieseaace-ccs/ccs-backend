import { MigrationInterface, QueryRunner } from 'typeorm';

export class createUserRoleTable1626372127199 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`CREATE TABLE dbo.user_role
    (
        user_id uniqueidentifier not null
            constraint FK_d0e5815877f7395a198a4cb0a46
                references dbo.users
                    on update cascade on delete cascade,
        role_id int not null
            constraint FK_32a6fc2fcb019d8e3a8ace0f55f
                references dbo.roles
                    on update cascade on delete cascade,
          created_at datetime2 constraint DF_c9b5b525a96ddc2c5647d7f7fa6 default getdate() not null,
          updated_at datetime2 constraint DF_6d596d799f9cb9dac6f7bf7c23d default getdate() not null,
          deleted_at datetime2,  
        constraint PK_f634684acb47c1a158b83af5150
            primary key (user_id, role_id)
    )`);

    await queryRunner.query(
      `CREATE INDEX IDX_d0e5815877f7395a198a4cb0a4 on dbo.user_role (user_id)`
    );

    await queryRunner.query(
      `CREATE INDEX IDX_32a6fc2fcb019d8e3a8ace0f55 on dbo.user_role (role_id)`
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`DROP TABLE dbo.user_role`);
  }
}
