import { MigrationInterface, QueryRunner } from 'typeorm';

export class createCampaignsTable1633407935150 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`CREATE TABLE campaigns
    (
        id int identity,
        uuid uniqueidentifier constraint DF_87b561983a107d4521bcd946222 default newsequentialid() not null,
        campaign nvarchar(100) not null,
        skill nvarchar(100) not null,
        created_at datetime2 constraint DF_e787e23857eab09fbffacddd23b default getdate() not null,
        constraint PK_942926fe4d3e40116d21c2c9bb3 primary key (id, uuid)
    )`);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`DROP TABLE campaigns`);
  }
}
