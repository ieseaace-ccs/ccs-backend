import * as Joi from 'joi';

export const configSchema = Joi.object({
  SENTRY_DSN: Joi.string().required(),
  CAMPAIGNS_MICROSERVICE_HOST: Joi.string().default('localhost'),
  CAMPAIGNS_MICROSERVICE_PORT: Joi.number().required(),
  CAMPAIGNS_DB_TYPE: Joi.string().required(),
  CAMPAIGNS_DB_HOST: Joi.string().required(),
  CAMPAIGNS_DB_PORT: Joi.number().required(),
  CAMPAIGNS_DB_USERNAME: Joi.string().required(),
  CAMPAIGNS_DB_PASSWORD: Joi.string(),
  CAMPAIGNS_DB_NAME: Joi.string().required(),
  CAMPAIGNS_DB_RUN_MIGRATIONS: Joi.number(),
  CAMPAIGNS_DB_SYNC: Joi.number(),
  CAMPAIGNS_DB_TRUST_CERTIFICATE: Joi.number().required(),
  AUTH_MICROSERVICE_HOST: Joi.string().default('localhost'),
  AUTH_MICROSERVICE_PORT: Joi.number().required()
});
