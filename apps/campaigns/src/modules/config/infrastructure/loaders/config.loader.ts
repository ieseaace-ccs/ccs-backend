import { DatabaseConfigType } from '../types/database.type';
import { ServerConfigType } from '../types/server.type';
import { SentryConfigType } from './../types/sentry.type';
import { ServicesConfigType } from '../types/services.type';

export const configLoader = (): ConfigLoader => ({
  server: {
    host: process.env.CAMPAIGNS_MICROSERVICE_HOST,
    port: parseInt(process.env.CAMPAIGNS_MICROSERVICE_PORT, 10)
  },
  database: {
    type: process.env.CAMPAIGNS_DB_TYPE,
    host: process.env.CAMPAIGNS_DB_HOST,
    port: parseInt(process.env.CAMPAIGNS_DB_PORT, 10),
    username: process.env.CAMPAIGNS_DB_USERNAME,
    password: process.env.CAMPAIGNS_DB_PASSWORD,
    database: process.env.CAMPAIGNS_DB_NAME,
    synchronize: process.env.CAMPAIGNS_DB_SYNC === '1',
    autoLoadEntities: true,
    migrationsTableName: 'migrations',
    extra: {
      trustServerCertificate:
        process.env.CAMPAIGNS_DB_TRUST_CERTIFICATE === '1',
      requestTimeout: 0
    }
  },
  sentry: {
    sentryDsn: process.env.SENTRY_DSN,
    sentryEnvironment: process.env.ENVIRONMENT
  },
  services: {
    auth: {
      host: process.env.AUTH_MICROSERVICE_HOST,
      port: process.env.AUTH_MICROSERVICE_PORT
    }
  }
});

type ConfigLoader = {
  server: ServerConfigType;
  database: DatabaseConfigType;
  sentry: SentryConfigType;
  services: ServicesConfigType;
};
