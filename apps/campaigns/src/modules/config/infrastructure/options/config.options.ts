import * as path from 'path';
import { ConfigModuleOptions } from '@nestjs/config/dist/interfaces';

import { configSchema } from '../schemas/config.schema';
import {
  serverConfigLoader,
  databaseConfigLoader,
  sentryConfigLoader,
  servicesConfigLoader
} from '../loaders';

export const options: ConfigModuleOptions = {
  envFilePath: path.join(__dirname.replace(/\/dist/, ''), '.env'),
  cache: true,
  isGlobal: true,
  load: [
    serverConfigLoader,
    databaseConfigLoader,
    sentryConfigLoader,
    servicesConfigLoader
  ],
  validationSchema: configSchema,
  validationOptions: {
    allowUnknown: true,
    abortEarly: true
  }
};
