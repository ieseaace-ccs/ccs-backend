import {
  BaseEntity,
  Column,
  CreateDateColumn,
  Entity,
  PrimaryGeneratedColumn
} from 'typeorm';

@Entity('campaigns')
export class CampaignsEntity extends BaseEntity {
  @PrimaryGeneratedColumn('increment')
  id: number;

  @PrimaryGeneratedColumn('uuid')
  uuid: string;

  @Column('nvarchar', { length: 100 })
  campaign: string;

  @Column('nvarchar', { length: 100 })
  skill: string;

  @CreateDateColumn({ name: 'created_at' })
  createdAt: Date;
}
