import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { CampaignsEntity } from './domain/models/entities/campaigns.entity';

@Module({
  imports: [TypeOrmModule.forFeature([CampaignsEntity])]
})
export class CampaignsModule {}
