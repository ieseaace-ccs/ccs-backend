import path = require('path');
import fs = require('fs');
import * as dotenv from 'dotenv';

dotenv.config();

const relative: string = path.join(path.relative('.', __dirname), '..');

fs.writeFileSync(
  path.join(__dirname, '/../ormconfig.json'),
  JSON.stringify(
    {
      type: process.env.CAMPAIGNS_DB_TYPE,
      host: process.env.CAMPAIGNS_DB_HOST,
      port: parseInt(process.env.CAMPAIGNS_DB_PORT, 10),
      username: process.env.CAMPAIGNS_DB_USERNAME,
      password: process.env.CAMPAIGNS_DB_PASSWORD,
      database: process.env.CAMPAIGNS_DB_NAME,
      synchronize: process.env.CAMPAIGNS_DB_SYNC === '1',
      autoLoadEntities: true,
      migrationsTableName: 'migrations',
      extra: {
        trustServerCertificate:
          process.env.CAMPAIGNS_DB_TRUST_CERTIFICATE === '1'
      },
      cli: {
        migrationsDir: `${relative}/database/migration`
      },
      entities: [`${relative}/src/modules/**/*.entity.ts`],
      migrations: [`${relative}/database/migration/*.ts`]
    },
    null,
    2
  )
);
