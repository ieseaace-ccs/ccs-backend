import { UsersResponseDto } from './../../../../../../api-gateway/src/modules/users/infrastructure/dtos/user-response.dto';
import { plainToClass } from 'class-transformer';
import { Injectable, Inject } from '@nestjs/common';
import { ClientProxy } from '@nestjs/microservices';
import { JwtService } from '@nestjs/jwt';
import { compareSync } from 'bcrypt';

@Injectable()
export class AuthService {
  constructor(
    @Inject('USER_CLIENT') private readonly client: ClientProxy,
    private readonly jwtService: JwtService
  ) {}

  async validateUser(username: string, password: string): Promise<any> {
    const user = await this.client
      .send(
        { role: 'user', cmd: 'get' },
        { where: { username }, relations: ['roles'] }
      )
      .toPromise();

    if (user && compareSync(password, user?.password)) {
      return plainToClass(UsersResponseDto, user);
    }

    return null;
  }

  async login(user: UsersResponseDto) {
    const { uuid, status, process, username, roles } = user;
    const payload = {
      user: { uuid, status, process, username, roles },
      sub: user.uuid
    };

    return {
      userId: user.username,
      accessToken: this.jwtService.sign(payload)
    };
  }
  async changePassword(uuid: string, newPassword: string) {
    return this.client
      .send({ role: 'user', cmd: 'change-pass' }, { uuid, newPassword })
      .toPromise();
  }

  validateToken(jwt: string) {
    return this.jwtService.verify(jwt);
  }
}
