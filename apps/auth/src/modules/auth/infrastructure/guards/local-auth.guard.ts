import { Injectable, CanActivate, ExecutionContext } from '@nestjs/common';
import { RpcException } from '@nestjs/microservices';
import { AuthService } from '../../domain/services/auth.service';

@Injectable()
export class LocalAuthGuard implements CanActivate {
  constructor(private readonly authService: AuthService) {}
  async canActivate(context: ExecutionContext): Promise<boolean> {
    const request = context.switchToRpc().getData();
    const { username, password } = request;
    const user = await this.authService.validateUser(username, password);

    if (!user) {
      throw new RpcException({ message: 'Unauthorized', statusCode: 401 });
    }
    request.user = user;
    return user;
  }
}
