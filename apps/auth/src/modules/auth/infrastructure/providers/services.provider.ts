import { ClientProxyFactory, Transport } from '@nestjs/microservices';
import { ConfigService } from '@nestjs/config';

export const userClientProvider = {
  provide: 'USER_CLIENT',
  inject: [ConfigService],
  useFactory: (config: ConfigService) =>
    ClientProxyFactory.create({
      transport: Transport.TCP,
      options: {
        host: config.get('services').users.host,
        port: config.get('services').users.port
      }
    })
};
