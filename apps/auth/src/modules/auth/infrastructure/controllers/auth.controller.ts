import { Controller, UseGuards } from '@nestjs/common';
import { LocalAuthGuard } from '../guards/local-auth.guard';
import { AuthService } from '../../domain/services/auth.service';
import { MessagePattern, Payload } from '@nestjs/microservices';
import { LoginResponseDto } from '../../../../../../api-gateway/src/modules/auth/infrastructure/dtos/login-response';

@Controller()
export class AuthController {
  constructor(private readonly authService: AuthService) {}
  @UseGuards(LocalAuthGuard)
  @MessagePattern({ role: 'auth', cmd: 'login' })
  public async login(@Payload() loginData: any): Promise<LoginResponseDto> {
    return this.authService.login(loginData.user);
  }

  @UseGuards(LocalAuthGuard)
  @MessagePattern({ role: 'auth', cmd: 'change' })
  public async change(@Payload() data: any): Promise<LoginResponseDto> {
    const { user, newPassword } = data;
    return this.authService.changePassword(user.uuid, newPassword);
  }

  @MessagePattern({ role: 'auth', cmd: 'check' })
  public async loggedIn(data: any): Promise<boolean> {
    try {
      return await this.authService.validateToken(data.jwt);
    } catch (e) {
      return false;
    }
  }
}
