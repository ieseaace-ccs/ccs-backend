import { Module } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { JwtModule } from '@nestjs/jwt';

import { ConfigModule } from '../config/config.module';
import { AuthService } from './domain/services/auth.service';

import { AuthController } from './infrastructure/controllers/auth.controller';
import { userClientProvider } from './infrastructure/providers/services.provider';

@Module({
  imports: [
    JwtModule.registerAsync({
      imports: [ConfigModule],
      useFactory: async (config: ConfigService) => ({
        secret: config.get('application').secret,
        signOptions: { expiresIn: config.get('application').expiration }
      }),
      inject: [ConfigService]
    })
  ],
  controllers: [AuthController],
  providers: [AuthService, userClientProvider]
})
export class AuthModule {}
