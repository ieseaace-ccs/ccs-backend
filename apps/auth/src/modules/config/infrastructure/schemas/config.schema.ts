import * as Joi from 'joi';

export const configSchema = Joi.object({
  ENVIRONMENT: Joi.string().required(),
  SENTRY_DSN: Joi.string().required(),
  AUTH_MICROSERVICE_HOST: Joi.string().default('localhost'),
  AUTH_MICROSERVICE_PORT: Joi.number().required(),
  AUTH_MICROSERVICE_SECRET: Joi.string().required(),
  AUTH_MICROSERVICE_EXPIRATION: Joi.number().required(),
  USERS_MICROSERVICE_HOST: Joi.string().default('localhost'),
  USERS_MICROSERVICE_PORT: Joi.number().required()
});
