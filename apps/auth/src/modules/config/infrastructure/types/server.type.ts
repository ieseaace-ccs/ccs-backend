export type ServerConfigType = {
  host: string;
  port: number;
};
