export type ApplicationConfigType = {
  secret: string;
  expiration: number;
};
