import { ServerConfigType } from '../types/server.type';
import { SentryConfigType } from './../types/sentry.type';
import { ServicesConfigType } from './../types/services.type';
import { ApplicationConfigType } from '../types/application.type';

export const configLoader = (): ConfigLoader => ({
  server: {
    host: process.env.AUTH_MICROSERVICE_HOST,
    port: parseInt(process.env.AUTH_MICROSERVICE_PORT, 10)
  },
  sentry: {
    sentryDsn: process.env.SENTRY_DSN,
    sentryEnvironment: process.env.ENVIRONMENT
  },
  services: {
    users: {
      host: process.env.USERS_MICROSERVICE_HOST,
      port: process.env.USERS_MICROSERVICE_PORT
    }
  },
  application: {
    secret: process.env.AUTH_MICROSERVICE_SECRET,
    expiration: parseInt(process.env.AUTH_MICROSERVICE_EXPIRATION)
  }
});

type ConfigLoader = {
  server: ServerConfigType;
  sentry: SentryConfigType;
  services: ServicesConfigType;
  application: ApplicationConfigType;
};
