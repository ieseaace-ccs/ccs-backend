import { registerAs } from '@nestjs/config';

import { configLoader } from './config.loader';
import { SentryConfigType } from '../types/sentry.type';

export const sentryConfigLoader = registerAs(
  'sentry',
  (): SentryConfigType => configLoader().sentry
);
