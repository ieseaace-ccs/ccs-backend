import { ConfigService } from '@nestjs/config';
import { NestFactory } from '@nestjs/core';
import {
  BadRequestException,
  Logger,
  ValidationError,
  ValidationPipe
} from '@nestjs/common';
import {
  MicroserviceOptions,
  TcpClientOptions,
  Transport
} from '@nestjs/microservices';

import { MainModule } from './modules/main/main.module';
import { ServerConfigType } from './modules/config/infrastructure/types/server.type';

async function bootstrap() {
  const app = await NestFactory.create(MainModule);

  const configService: ConfigService = app.get('ConfigService');
  const config = configService.get<ServerConfigType>('server');
  const { host, port } = config;

  const options: TcpClientOptions = {
    transport: Transport.TCP,
    options: { host, port }
  };

  app.connectMicroservice<MicroserviceOptions>(options);

  app.useGlobalPipes(
    new ValidationPipe({
      exceptionFactory: (errors: ValidationError[]) => {
        const err = errors.map((el) => {
          return el.constraints;
        });
        throw new BadRequestException(err);
      }
    })
  );

  app.startAllMicroservices(() => {
    Logger.log(`TCP Server Listening on port ${port}`, 'Auth Microservice');
  });
}

bootstrap();
